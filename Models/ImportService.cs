﻿using Microsoft.AspNetCore.Http;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;

namespace SEO_System.Models
{
    public class ImportService
    {
        public ImportService()
        {
        }

        /// <summary>
        /// Cách lấy:  file = Request.Form.Files[0];
        /// </summary>
        /// <param name="file">IFormFile file = Request.Form.Files[0];</param>
        /// <returns>List<IRow> list_row: Danh sách tất cả các row trong Excel trừ dòng đầu tiên</returns>
        public List<IRow> ReadExcel(IFormFile file)
        {
            //IFormFile file = Request.Form.Files[0];
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            string sFileWillSave = file.FileName;
            string fileDirect = @"/home/amnhacsaigon/dotnet/seo-system/wwwroot/data-file/upload/";
            if (isDev)
            {
                fileDirect = @"C://upload";
            }
            if (!Directory.Exists(fileDirect))
            {
                Directory.CreateDirectory(fileDirect);
            }
            var path = Path.Combine(Directory.GetCurrentDirectory(), fileDirect, sFileWillSave);
            if (file.Length > 0)
            {
                string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                ISheet sheet;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyTo(stream);
                    stream.Position = 0;
                    if (sFileExtension == ".xls")
                    {
                        HSSFWorkbook hssfwb = new HSSFWorkbook(stream);
                        sheet = hssfwb.GetSheetAt(0);
                    }
                    else
                    {
                        XSSFWorkbook hssfwb = new XSSFWorkbook(stream);
                        sheet = hssfwb.GetSheetAt(0);
                    }
                    IRow headerRow = sheet.GetRow(0);
                    int cellCount = headerRow.LastCellNum;
                    // get header
                    for (int j = 0; j < cellCount; j++)
                    {
                        ICell cell = headerRow.GetCell(j);
                    }
                    // get content
                    List<IRow> list_row = new List<IRow>();
                    for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                    {
                        IRow row = sheet.GetRow(i);
                        list_row.Add(row);
                    }
                    if (list_row.Count > 0)
                    {
                        return list_row;
                    }
                }
            }
            return null;
        }
    }
}