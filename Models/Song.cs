﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Song
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int songId { get; set; }
        public string songName { get; set; }
        public string songDesc { get; set; }
        public string songLyric { get; set; }
        public string songYear { get; set; }
        public string songImage { get; set; }
        public string songFile { get; set; }
        public string songTags { get; set; }
        public DateTime songDateCreated { get; set; }
        public int songView { get; set; }
        public int cateId { get; set; }
        public int userId { get; set; }
        public int albumId { get; set; }
        public int countryId { get; set; }

        public Song GetSongBySongId(int songId)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Song>().Where(e => e.songId == songId);
                return db.Select(query).SingleOrDefault();
            }
        }

        public int Edit(int songId)
        {
            int result = -1;

            return result;
        }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ DANH MỤC =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Song(Bài hát)</summary>
        /// <returns>Trả về List<Song></returns>
        public List<Song> GetAllSong()
        {
            List<Song> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Song>();
                query.OrderByDescending(x => x.songName);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ DANH MỤC =====================================

        #region ===================================== XÓA THÔNG QUA songId s=====================================

        /// <summary>Xóa 1 danh mục qua songId</summary>
        /// <param name="songId">  Mã songId của danh mục muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int songId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Song>().Where(x => x.songId == songId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA songId s=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Song</summary>
        /// <param name="song">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Song song)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (song.songId > 0)
                {
                    var queryUpdate = db.From<Song>().Where(e => e.songId == song.songId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.songId = song.songId;
                        objUpdate.songName = song.songName;
                        objUpdate.songDesc = song.songDesc;
                        objUpdate.songLyric = song.songLyric;
                        objUpdate.songYear = song.songYear;
                        objUpdate.songImage = song.songImage;
                        objUpdate.songFile = song.songFile;
                        objUpdate.songTags = song.songTags;
                        objUpdate.songDateCreated = song.songDateCreated;
                        objUpdate.songView = song.songView;
                        objUpdate.cateId = song.cateId;
                        objUpdate.userId = song.userId;
                        objUpdate.countryId = song.countryId;
                        result = db.Update(objUpdate);
                    }
                    else
                    {
                        var objInsert = new Song
                        {
                            songId = song.songId,
                            songName = song.songName,
                            songDesc = song.songDesc,
                            songLyric = song.songLyric,
                            songYear = song.songYear,
                            songImage = song.songImage,
                            songFile = song.songFile,
                            songTags = song.songTags,
                            songDateCreated = song.songDateCreated,
                            songView = song.songView,
                            cateId = song.cateId,
                            userId = song.userId,
                            countryId = song.countryId
                        };
                        result = (int)db.Insert(objInsert, selectIdentity: true);
                    }
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
    }
}