﻿using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class ModelData
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int mdId { get; set; }
        public string mdType { get; set; }
        public string actType1 { get; set; }
        public string actTime1 { get; set; }
        public string actScale1 { get; set; }
        public string actType2 { get; set; }
        public string actTime2 { get; set; }
        public string actScale2 { get; set; }
        public string actType3 { get; set; }
        public string actTime3 { get; set; }
        public string actScale3 { get; set; }
        public string actType4 { get; set; }
        public string actTime4 { get; set; }
        public string actScale4 { get; set; }
        public string actType5 { get; set; }
        public string actTime5 { get; set; }
        public string actScale5 { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ MODEL =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Model</summary>
        /// <returns>Trả về List Model</returns>
        public List<ModelData> GetAllModelData()
        {
            List<ModelData> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<ModelData>();
                query.OrderByDescending(x => x.mdId);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<ModelData> Get(int mdId)
        {
            List<ModelData> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<ModelData>();
                if (mdId > 0) { query = query.Where(e => e.mdId == mdId); }
                query.OrderByDescending(x => x.mdId);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<ModelData> List(int _limit, int _offset, string _search)
        {
            List<ModelData> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<ModelData>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.mdType == search || e.mdType.Contains(search));
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.mdId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ MODEL =====================================

        #region ===================================== XÓA THÔNG QUA modelId=====================================

        /// <summary>Xóa 1 danh mục qua modelId</summary>
        /// <param name="mdId">  Mã mdId của id muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int mdId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<ModelData>().Where(x => x.mdId == mdId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA modelId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Model</summary>
        /// <param name="md">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(ModelData md)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (md.mdId > 0)
                {
                    var queryUpdate = db.From<ModelData>().Where(e => e.mdId == md.mdId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.mdId = md.mdId;
                        objUpdate.mdType = md.mdType;
                        objUpdate.actType1 = md.actType1;
                        objUpdate.actTime1 = md.actTime1;
                        objUpdate.actScale1 = md.actScale1;
                        objUpdate.actType2 = md.actType2;
                        objUpdate.actTime2 = md.actTime2;
                        objUpdate.actScale2 = md.actScale2;
                        objUpdate.actType3 = md.actType3;
                        objUpdate.actTime3 = md.actTime3;
                        objUpdate.actScale3 = md.actScale3;
                        objUpdate.actType4 = md.actType4;
                        objUpdate.actTime4 = md.actTime4;
                        objUpdate.actScale4 = md.actScale4;
                        objUpdate.actType5 = md.actType5;
                        objUpdate.actTime5 = md.actTime5;
                        objUpdate.actScale5 = md.actScale5;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new ModelData
                    {
                        mdType = md.mdType,
                        actType1 = md.actType1,
                        actTime1 = md.actTime1,
                        actScale1 = md.actScale1,
                        actType2 = md.actType2,
                        actTime2 = md.actTime2,
                        actScale2 = md.actScale2,
                        actType3 = md.actType3,
                        actTime3 = md.actTime3,
                        actScale3 = md.actScale3,
                        actType4 = md.actType4,
                        actTime4 = md.actTime4,
                        actScale4 = md.actScale4,
                        actType5 = md.actType5,
                        actTime5 = md.actTime5,
                        actScale5 = md.actScale5,
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
    }
}