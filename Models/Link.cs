﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Link
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int linkId { get; set; }
        public string linkName { get; set; }
        public string linkDesc { get; set; }
        public DateTime linkCreated { get; set; }
        public DateTime linkUpdated { get; set; }
        public string linkType { get; set; }
        public string linkStatus { get; set; }
        public string linkLog { get; set; }
        public int userId { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ Link =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Link</summary>
        /// <returns>Trả về List Link</returns>
        public List<Link> GetAllLink()
        {
            List<Link> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Link>();
                query.OrderByDescending(x => x.linkId);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Link> Get(int linkId, string linkName)
        {
            List<Link> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Link>();
                if (linkId > 0) { query = query.Where(e => e.linkId == linkId); }
                if (!string.IsNullOrEmpty(linkName)) { query = query.Where(e => e.linkName == linkName || e.linkName.Contains(linkName) || e.linkDesc.Contains(linkName)); }
                query.OrderByDescending(x => x.linkId);
                rows = db.Select(query).ToList();
            }
            return rows;
        }

        public Dictionary<object, object> BatchDelete(int[] list_selected)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "error", code = "Chưa có item nào được chọn để xóa";
            if (list_selected.Length > 0)
            {
                for (int i = 0; i < list_selected.Length; i++)
                {
                    Delete(list_selected[i]);
                }
                code = "success";
                message = "Xóa thành công";
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        public List<Link> List(int _limit, int _offset, string _search)
        {
            List<Link> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Link>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.linkName == search || e.linkName.Contains(search) || e.linkDesc == search || e.linkDesc.Contains(search));
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.linkId);
                rows = db.Select(query);
            }
            return rows;
        }

        public Link GetById(int linkId)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Link>().Where(e => e.linkId == linkId);
                return db.Select(query).SingleOrDefault();
            }
        }

        #endregion ===================================== LẤY TẤT CẢ Link =====================================

        #region ===================================== XÓA THÔNG QUA linkId=====================================

        /// <summary>Xóa 1 Link qua linkId</summary>
        /// <param name="linkId">  Mã linkId của Link muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int linkId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Link>().Where(x => x.linkId == linkId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA linkId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Link</summary>
        /// <param name="link">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Link link)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (link.linkId > 0)
                {
                    var queryUpdate = db.From<Link>().Where(e => e.linkId == link.linkId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.linkId = link.linkId;
                        objUpdate.linkName = link.linkName;
                        objUpdate.linkDesc = link.linkDesc;
                        objUpdate.linkCreated = link.linkCreated;
                        objUpdate.linkUpdated = link.linkUpdated;
                        objUpdate.linkType = link.linkType;
                        objUpdate.linkStatus = link.linkStatus;
                        objUpdate.linkLog = link.linkLog;
                        objUpdate.userId = link.userId;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Link()
                    {
                        linkName = link.linkName,
                        linkDesc = link.linkDesc,
                        linkCreated = link.linkCreated,
                        linkUpdated = link.linkUpdated,
                        linkType = link.linkType,
                        linkStatus = link.linkStatus,
                        linkLog = link.linkLog,
                        userId = link.userId
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
    }
}