﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Log
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int logId { get; set; }
        public string logCode { get; set; }
        public string logDesc { get; set; }
        public DateTime logCreated { get; set; }
        public DateTime logUpdated { get; set; }

        public Dictionary<object, object> UpdateLog(string logDesc)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "Cập nhật log thất bại", code = "error";
            string today_code = DateTime.Now.ToString("dd-MM-yyyy");
            int result = UpdateOrInsert(new Log()
            {
                logCode = today_code,
                logDesc = logDesc,
            });
            if (result > 0)
            {
                code = "success";
                message = "Cập nhật log thành công";
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ Log =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Log</summary>
        /// <returns>Trả về List Log</returns>
        public List<Log> GetAllLog()
        {
            List<Log> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Log>();
                query.OrderByDescending(x => x.logId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ Log =====================================

        #region ===================================== Get =====================================

        public List<Log> Get(int logId, string logCode)
        {
            List<Log> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Log>();
                if (logId > 0) { query = query.Where(e => e.logId == logId); }
                if (!string.IsNullOrEmpty(logCode)) { query = query.Where(e => e.logCode == logCode); }
                rows = db.Select(query).ToList();
            }
            return rows;
        }

        #endregion ===================================== Get =====================================

        #region ===================================== Get =====================================

        public Log GetByLogId(int LogId)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Log>();
                if (LogId > 0) { query = query.Where(e => e.logId == logId); }
                return db.Select(query).LastOrDefault();
            }
        }

        #endregion ===================================== Get =====================================

        #region ===================================== Batch Delete =====================================

        public Dictionary<object, object> BatchDelete(int[] list_selected)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "error", code = "Chưa có item nào được chọn để xóa";
            if (list_selected.Length > 0)
            {
                for (int i = 0; i < list_selected.Length; i++)
                {
                    Delete(list_selected[i]);
                }
                code = "success";
                message = "Xóa thành công";
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        #endregion ===================================== Batch Delete =====================================

        #region ===================================== Batch Delete =====================================

        public List<Log> SearchLog(int _limit, int _offset, string _search)
        {
            List<Log> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Log>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.logCode == search || e.logCode.Contains(search) || e.logDesc == search || e.logDesc.Contains(search) || e.logId.ToString() == search);
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.logId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== Batch Delete =====================================

        public Log GetById(int logId)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Log>().Where(e => e.logId == logId);
                return db.Select(query).SingleOrDefault();
            }
        }

        #region ===================================== XÓA THÔNG QUA LogId=====================================

        /// <summary>Xóa 1 Log qua LogId</summary>
        /// <param name="logId">  Mã LogId của Log muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int logId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Log>().Where(x => x.logId == logId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA LogId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Log</summary>
        /// <param name="Log">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Log log)
        {
            int result = -1;
            string today_code = DateTime.Now.ToString("dd-MM-yyyy");
            string newline = Environment.NewLine;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // Check nếu đã tồn tại logCode thời điểm hiện tại thì update thêm vào log của ngày đó
                if (!string.IsNullOrEmpty(log.logCode) && log.logCode == today_code)
                {
                    var rsCheck = db.Select(db.From<Log>().Where(e => e.logCode == log.logCode)).SingleOrDefault();
                    if (rsCheck != null)
                    {
                        log.logId = rsCheck.logId;
                        log.logCreated = rsCheck.logCreated;
                    }
                }
                // CODE UPDATE
                if (log.logId > 0)
                {
                    var queryUpdate = db.From<Log>().Where(e => e.logId == log.logId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        string old_logDesc = objUpdate.logDesc;
                        objUpdate.logId = log.logId;
                        objUpdate.logCode = log.logCode;
                        objUpdate.logDesc = old_logDesc + newline + DateTime.Now + ": " + log.logDesc;
                        objUpdate.logCreated = log.logCreated;
                        objUpdate.logUpdated = DateTime.Now;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Log()
                    {
                        logCode = log.logCode,
                        logDesc = DateTime.Now + ": " + log.logDesc,
                        logCreated = DateTime.Now,
                        logUpdated = DateTime.Now,
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
    }
}