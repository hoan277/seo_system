﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Actions
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int actId { get; set; }
        public string actName { get; set; }
        public string actDesc { get; set; }
        public string actType { get; set; }
        public string actLink { get; set; }
        public string actComment { get; set; }
        public string actTime { get; set; }
        public string actDestination { get; set; }
        public string actImages { get; set; }
        public string actReaction { get; set; }
        public string actStatus { get; set; }
        public string actMode { get; set; }
        public int userId { get; set; }
        public int timelineId { get; set; }
        public string actTo { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ DANH MỤC =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Danh mục</summary>
        /// <returns>Trả về List Actions</returns>
        public List<Actions> GetAllActions()
        {
            List<Actions> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Actions>();
                query.OrderByDescending(x => x.actName);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Actions> List(int _limit, int _offset, string _search)
        {
            List<Actions> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Actions>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e =>
                       e.actName == search || e.actName.Contains(search)
                    || e.actLink == search || e.actLink.Contains(search)
                    || e.actComment == search || e.actComment.Contains(search)
                );
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.actId);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Actions> GetByTimelineId(int _limit, int _offset, string _search, int timelineId)
        {
            List<Actions> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Actions>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.timelineId == timelineId);
                query = query.Where(e =>
                       e.actName == search || e.actName.Contains(search)
                    || e.actLink == search || e.actLink.Contains(search)
                    || e.actComment == search || e.actComment.Contains(search)
                    || e.actType.Contains(search)
                );
                query.Skip(offset).Take(limit);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Actions> Get(int actId, string actName, string actDesc, string actType, int userId, string actStatus, int timelineId)
        {
            List<Actions> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Actions>();
                if (actId > 0) { query = query.Where(a => a.actId == actId); }
                if (timelineId > 0) { query = query.Where(a => a.timelineId == timelineId); }
                if (userId > 0) { query = query.Where(a => a.userId == userId); }
                if (!string.IsNullOrEmpty(actName)) { query = query.Where(a => a.actName == actName || a.actDesc.Contains(actName)); }
                if (!string.IsNullOrEmpty(actDesc)) { query = query.Where(a => a.actDesc == actDesc || a.actDesc.Contains(actDesc)); }
                if (!string.IsNullOrEmpty(actStatus)) { query = query.Where(a => a.actStatus == actStatus || a.actStatus.Contains(actStatus)); }
                query.OrderByDescending(x => x.actId);
                rows = db.Select(query).ToList();
            }
            return rows;
        }

        public List<Actions> GetActionByTimelineId(int timelineId, string actMode)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Actions>();
                if (timelineId > 0) { query = query.Where(a => a.timelineId == timelineId); }
                if (!string.IsNullOrEmpty(actMode)) { query = query.Where(a => a.actMode == actMode); }
                return db.Select(query).ToList();
            }
        }

        #endregion ===================================== LẤY TẤT CẢ DANH MỤC =====================================

        #region ===================================== XÓA THÔNG QUA actId=====================================

        /// <summary>Xóa 1 Actions qua actId</summary>
        /// <param name="actId">  Mã actId của Actions muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int actId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Actions>().Where(x => x.actId == actId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA actId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Actions</summary>
        /// <param name="act">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Actions act)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (act.actId > 0)
                {
                    var queryUpdate = db.From<Actions>().Where(e => e.actId == act.actId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.actId = act.actId;
                        objUpdate.actName = act.actName;
                        objUpdate.actDesc = act.actDesc;
                        objUpdate.actType = act.actType;
                        objUpdate.actLink = act.actLink;
                        objUpdate.actComment = act.actComment;
                        objUpdate.actTime = act.actTime;
                        objUpdate.actDestination = act.actDestination;
                        objUpdate.actImages = act.actImages;
                        objUpdate.actReaction = act.actReaction;
                        objUpdate.actStatus = act.actStatus;
                        objUpdate.actMode = act.actMode;
                        objUpdate.userId = act.userId;
                        objUpdate.timelineId = act.timelineId;
                        objUpdate.actTo = act.actTo;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Actions
                    {
                        actName = act.actName,
                        actDesc = act.actDesc,
                        actType = act.actType,
                        actLink = act.actLink,
                        actComment = act.actComment,
                        actTime = act.actTime,
                        actDestination = act.actDestination,
                        actImages = act.actImages,
                        actReaction = act.actReaction,
                        actStatus = act.actStatus,
                        actMode = act.actMode,
                        userId = act.userId,
                        actTo = act.actTo,
                        timelineId = act.timelineId,
                    };

                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        #region ===================================== CẬP NHẬT LOG =====================================

        public Dictionary<object, object> UpdateLog(string log, int actId)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "Cập nhật log thất bại", code = "error";
            try
            {
                using (var db = DatabaseUtils.OpenConnection())
                {
                    if (actId > 0)
                    {
                        var queryUpdate = db.From<Actions>().Where(e => e.actId == actId);
                        var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                        if (objUpdate != null)
                        {
                            string newLine = Environment.NewLine;
                            var old_log = objUpdate.actMode;
                            objUpdate.actMode = "--- [" + DateTime.Now + "] --- " + log + newLine + old_log;
                            try
                            {
                                if (db.Update(objUpdate) > 0)
                                {
                                    message = "Cập nhật log thành công";
                                    code = "success";
                                }
                            }
                            catch (Exception ex)
                            {
                                message = "Cập nhật log thất bại: " + ex.Message;
                                code = "error";
                            }
                        }
                        else
                        {
                            message = "Không tìm thấy action này";
                            code = "error";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = "Không kết nối được đến cơ sở dữ liệu hoặc " + ex.Message;
                code = "error";
            }
            dict.Add("code", code); dict.Add("message", message);
            return dict;

        }
        #endregion ===================================== CẬP NHẬT LOG =====================================

        #region ===================================== GenAuto =====================================
        public Dictionary<object, object> GenAuto(string modeTest, List<string> genPlatform, List<string> genActions, float genAchived, string genLink, string genKeyword, string comGroup, int userId, string actMode, string vpsGroup)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string code = "error", message = "Thất bại";
            int maxAchived = 0;
            int accId = 0;
            string vpsIp = "";
            int uaId = 0;
            int actTime = 0;
            int countTime = 0;
            List<object> resultTL = new List<object>();
            List<object> resultAct = new List<object>();
            int count = 1;
            int tlRepeat = 0;
            int maxTime = 60;
            int minTime = 45; // Thời gian nhỏ nhất của 1 lần view
            string genComment = "";

            // Tỷ lệ % like/dislike/comment
            float like_percent = (genAchived / 100) * 10;   //  100
            float dislike_percent = (genAchived / 100) * 5; //  50
            float comment_percent = (genAchived / 100) * 3; //  70
            bool check_to_view_only = false;
            // Biến lưu danh sách tài khoản và vps đã sử dụng lại để tránh lặp lại
            List<Account> accUsed = new List<Account>();
            List<Vps> vpsUsed = new List<Vps>();
            List<UserAgent> uaUsed = new List<UserAgent>();
            List<Comment> comUsed = new List<Comment>();

            // Test
            //int newTlid = 0;
            int actId = 1;
            int count_like = (int)like_percent;
            int count_dislike = (int)dislike_percent;
            int count_comment = (int)comment_percent;

            // Tính % các platform (mặc định để 100% theo từng platform)
            float youtube_percent = (genAchived / 100) * 100;
            float google_percent = (genAchived / 100) * 100;
            float facebook_percent = (genAchived / 100) * 100;
            float instagram_percent = (genAchived / 100) * 100;

            if (genPlatform.Contains("youtube") && genPlatform.Contains("google"))
            {
                youtube_percent = (genAchived / 100) * 93;
                google_percent = (genAchived / 100) * 7;
            }
            if (genPlatform.Contains("youtube") && genPlatform.Contains("google") && genPlatform.Contains("facebook"))
            {
                youtube_percent = (genAchived / 100) * 83;
                google_percent = (genAchived / 100) * 7;
                facebook_percent = (genAchived / 100) * 10;
            }
            // Nếu chọn tất cả các platform
            if (genPlatform.Count() == 4)
            {
                youtube_percent = (genAchived / 100) * 73;
                google_percent = (genAchived / 100) * 7;
                facebook_percent = (genAchived / 100) * 10;
                instagram_percent = (genAchived / 100) * 10;
            }
            if (genPlatform.Contains("all"))
            {
                youtube_percent = (genAchived / 100) * 70;
                google_percent = (genAchived / 100) * 30;
                genPlatform.Add("youtube");
                genPlatform.Add("google");
            }

            try
            {
                List<Vps> allVps = new Vps().Get(0, "", "", "active", "", "", vpsGroup);
                if (allVps.Count <= 0)
                {
                    code = "error";
                    message = "Không có VPS nào được tìm thấy với lựa chọn ở trên";
                    goto lblResult;
                }
                List<UserAgent> allUa = new UserAgent().Get(0, "", "active");
                if (allUa.Count <= 0)
                {
                    code = "error";
                    message = "Không có UserAgent nào được tìm thấy";
                    goto lblResult;
                }
                Actions actObject = new Actions();
                // Lấy VPS theo Group

                // Lấy Comment theo Group
                List<Comment> allComment = new Comment().Get(0, "", "active", "", comGroup);
                if (allComment.Count <= 0)
                {
                    code = "error";
                    message = "Không có Comment nào được tìm thấy";
                    goto lblResult;
                }
                /*=========================================== TÍNH TỶ LỆ PHẦN TRĂM LIKE - DISLIKE - COMMENT =========================================== */

                #region ================== YOUTUBE ==================

                if (genPlatform.Contains("youtube"))
                {
                    List<Account> allAccByPlatform = new Account().Get(0, "", "active", "youtube", "");
                    if (allAccByPlatform.Count <= 0)
                    {
                        code = "error";
                        message = "Không có tài khoản Youtube nào được tìm thấy";
                        goto lblResult;
                    }
                    List<Account> accVip = new List<Account>();
                    foreach (var item in allAccByPlatform)
                    {
                        if (item.accNote.Contains("vip") || item.accNote == "VIP")
                        {
                            accVip.Add(item);
                        }
                    }
                    // Tạo mới Timeline + Actions cho 10% like
                    // like_percent lúc này 20% của 1000 =======> 100 cái Timeline đầy đủ bao gồm 100 like + 70 comment
                    // like_percent = 100
                    // 70 cái comment

                    #region ========================== Tạo mới 200 action yt_search và yt_view và yt_like mặc định ==========================
                    for (int i = 0; i < (int)youtube_percent; i++)
                    {
                        // Lấy ra tài khoản theo nền tảng đã chọn: Youtube/Google/Facebook -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
                        if (allAccByPlatform.Count <= 0) { allAccByPlatform = accUsed; }
                        accId = allAccByPlatform[0].accId;
                        accUsed.Add(allAccByPlatform[0]);
                        allAccByPlatform.RemoveAt(0);
                        // Lấy VPS -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
                        if (allVps.Count <= 0) { allVps = vpsUsed; }
                        vpsIp = allVps[0].vpsIp;
                        vpsUsed.Add(allVps[0]);
                        allVps.RemoveAt(0);
                        // Lấy UserAgent -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
                        if (allUa.Count <= 0) { allUa = uaUsed; }
                        uaId = allUa[0].uaId;
                        uaUsed.Add(allUa[0]);
                        allUa.RemoveAt(0);
                        // Lấy Comment theo Group

                        // Lấy Comment -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
                        if (allComment.Count <= 0) { allComment = comUsed; }
                        genComment = allComment[0].comDesc;
                        comUsed.Add(allComment[0]);
                        allComment.RemoveAt(0);


                        // Random từ min 45s - 120s trở đi để Youtube có thể tính là 1 lượt view
                        actTime = new Random().Next(minTime, maxTime);


                        // Check khi chỉ có view thì sẽ không + thêm thời gian để test vì nó có repeat khác server, khác useragent, khác tài khoản rồi
                        if (check_to_view_only)
                        {
                            actTime = 0;
                        }
                        else
                        {
                            countTime += actTime;
                        }

                        //newTlid += 1;
                        // Tạo ra số chỉ có Actions yt_view còn lại vì đã tạo xong các Timeline chứa Like, Comment rồi
                        // Check chế độ Test 1 - Test 2
                        int minRepeat = 20;
                        if (modeTest == "test1")
                        {
                            if (check_to_view_only)
                            {
                                // Nếu số lượng cần đạt còn nhiều thì phải repeat
                                // Nếu ít hơn thì set bằng 0 luôn
                                int count_re = (int)genAchived - (int)maxAchived;

                                tlRepeat = count_re > minRepeat ? new Random().Next(0, minRepeat) : 0;
                                genAchived -= tlRepeat;
                                if (actTime > 0)
                                {
                                    countTime *= tlRepeat;
                                }
                            }
                        }
                        else
                        {
                            countTime += actTime;
                            tlRepeat = 0;
                        }

                        if ((genActions.Contains("like") && i < like_percent) || (genActions.Contains("comment") && i < comment_percent))
                        {
                            accId = accVip[new Random().Next(0, accVip.Count)].accId;
                        }

                        // Tạo mới 1 Timeline
                        Timeline tl = new Timeline() { timelineName = string.Format("{0} {1} - {2}", "Auto Gen Youtube", count++, DateTime.Now.ToString("yyyyMMdd-") + Guid.NewGuid().ToString().Substring(0, 5)), timelineType = "youtube", timelineStatus = "deactive", userId = userId, accId = accId, uaId = uaId, vpsIp = vpsIp, timelineDelay = 0, timelineRepeat = tlRepeat, };
                        resultTL.Add(tl);
                        int newTlid = tl.UpdateOrInsert(tl);
                        tl.timelineId = newTlid;
                        if (genActions.Contains("search"))
                        {
                            //  100 yt_search mặc định
                            Actions yt_search = new Actions() { actName = "Search " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_search", actTime = "0", actStatus = "active", userId = userId, actDesc = genKeyword, actMode = actMode, };
                            actObject.UpdateOrInsert(yt_search);
                            resultAct.Add(yt_search);
                        }
                        if (genActions.Contains("view"))
                        {
                            // 100 yt_view mặc định
                            Actions yt_view = new Actions() { actName = "View " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_view", actTime = actTime.ToString(), actStatus = "active", userId = userId, actMode = actMode, actDesc = genKeyword, };
                            maxAchived += 1;
                            actObject.UpdateOrInsert(yt_view);
                            resultAct.Add(yt_view);
                        }
                        if (genActions.Contains("back"))
                        {
                            // Thêm Actions yt_back của số lượng còn lại:  yt_search ---> yt_view ---> yt_back
                            if (check_to_view_only)
                            {
                                // Random yt_back
                                Actions act_back = new Actions() { actName = "Back " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_back", actTime = "0", actStatus = "active", userId = userId, actMode = actMode, };
                                resultAct.Add(act_back);
                                int newActId = actObject.UpdateOrInsert(act_back);
                            }
                        }
                        if (genActions.Contains("like"))
                        {
                            // Chạy đến đây thì đã có 100 yt_search +  100 yt_view + 100 yt_login + 100 yt_like trong tổng 100 Timeline login_like đầu tiên
                            // 70 yt_comment
                            // Vì 10% của 1000 = 100 cái Actions yt_login + 100 yt_like
                            if (i < like_percent)
                            {
                                // 100 yt_login mặc định
                                Actions act_login = new Actions() { actName = "Login " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_login", actTime = "0", actStatus = "active", userId = userId, };
                                resultAct.Add(act_login);
                                int newActId = actObject.UpdateOrInsert(act_login);

                                // 100 yt_like
                                Actions yt_like = new Actions() { actName = "Like " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_like", actTime = "0", actStatus = "active", userId = userId, actMode = actMode, };
                                maxAchived += 1;
                                resultAct.Add(yt_like);
                                actObject.UpdateOrInsert(yt_like);
                                count_like--;
                            }
                        }
                        if (genActions.Contains("comment"))
                        {
                            // Chạy đến đây thì đã có 100 yt_search +  100 yt_view + 100 yt_login + 100 yt_like trong tổng 100 Timeline login_like đầu tiên
                            // 70 yt_comment
                            // Vì 7% của 1000 = 70 cái Actions yt_comment
                            if (i < comment_percent)
                            {
                                Actions yt_comment = new Actions() { actName = "Comment " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_comment", actTime = "0", actStatus = "active", userId = userId, actComment = genComment, actMode = actMode };
                                maxAchived += 1;
                                resultAct.Add(yt_comment);
                                actObject.UpdateOrInsert(yt_comment);
                                count_comment--;
                                // Random yt_back
                                Actions act_back = new Actions() { actName = "Back " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_back", actTime = "0", actStatus = "active", userId = userId, };
                                resultAct.Add(act_back);
                                int newActId = actObject.UpdateOrInsert(act_back);
                                if (count_comment <= 0)
                                {
                                    continue;
                                }
                            }
                        }
                        #endregion
                        if (genActions.Contains("dislike"))
                        {
                            // Tạo ra 50 cái yt_dislike
                            if (count_like <= 0 && count_comment <= 0 && count_dislike > 0)
                            {
                                // 100 yt_login mặc định
                                Actions act_login = new Actions() { actName = "Login " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_login", actTime = "0", actStatus = "active", userId = userId, };
                                resultAct.Add(act_login);
                                actObject.UpdateOrInsert(act_login);
                                // Tạo mới 1 action dislike đối thủ trước khi vào like của mình
                                Actions yt_dislike = new Actions() { actName = "Dislike " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_dislike", actTime = "0", actStatus = "active", userId = userId, actMode = actMode, };
                                maxAchived += 1;
                                resultAct.Add(yt_dislike);
                                actObject.UpdateOrInsert(yt_dislike);
                                count_dislike--;
                                if (count_dislike <= 0)
                                {
                                    check_to_view_only = true;
                                }
                                // Random yt_back
                                Actions act_back = new Actions() { actName = "Back " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_back", actTime = "0", actStatus = "active", userId = userId, };
                                resultAct.Add(act_back);
                                int newActId = actObject.UpdateOrInsert(act_back);
                            }
                        }
                    }
                }

                #endregion


                #region ================== Google ==================
                if (genPlatform.Contains("google"))
                {
                    List<Account> allAccByPlatform = new Account().Get(0, "", "active", "youtube", "");

                    if (allAccByPlatform.Count() <= 0)
                    {
                        code = "error";
                        message = "Không có tài khoản Google nào";
                        goto lblResult;
                    }
                    for (int i = 0; i < google_percent; i++)
                    {
                        // Lấy ra tài khoản theo nền tảng đã chọn: Youtube/Google/Facebook -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
                        if (allAccByPlatform.Count <= 0) { allAccByPlatform = accUsed; }
                        accId = allAccByPlatform[0].accId;
                        accUsed.Add(allAccByPlatform[0]);
                        allAccByPlatform.RemoveAt(0);
                        // Lấy VPS -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
                        if (allVps.Count <= 0) { allVps = vpsUsed; }
                        vpsIp = allVps[0].vpsIp;
                        vpsUsed.Add(allVps[0]);
                        allVps.RemoveAt(0);
                        // Lấy UserAgent -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
                        if (allUa.Count <= 0) { allUa = uaUsed; }
                        uaId = allUa[0].uaId;
                        uaUsed.Add(allUa[0]);
                        allUa.RemoveAt(0);

                        // Tạo mới 1 Timeline Google
                        Timeline tl = new Timeline() { timelineName = string.Format("{0} {1} - {2}", "Auto Gen Google", count++, DateTime.Now.ToString("yyyyMMdd-") + Guid.NewGuid().ToString().Substring(0, 5)), timelineType = "google", timelineStatus = "deactive", userId = userId, accId = accId, uaId = uaId, vpsIp = vpsIp, timelineDelay = 0, timelineRepeat = tlRepeat, };
                        resultTL.Add(tl);
                        int newTlid = tl.UpdateOrInsert(tl);
                        tl.timelineId = newTlid;
                        //  100 gg_search mặc định
                        Actions gg_search = new Actions() { actName = "Search " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "gg_search", actTime = "0", actStatus = "active", userId = userId, actDesc = genKeyword, };
                        actObject.UpdateOrInsert(gg_search);
                        resultAct.Add(gg_search);

                        //  100 gg_back mặc định
                        Actions gg_back = new Actions() { actName = "Back " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "gg_back", actTime = "0", actStatus = "active", userId = userId, };
                        actObject.UpdateOrInsert(gg_back);
                        resultAct.Add(gg_back);
                    }
                }
                #endregion

                #region ================== Facebook ==================
                if (genPlatform.Contains("facebook"))
                {
                    List<Account> allAccByPlatform = new Account().Get(0, "", "active", "facebook", "");

                    if (allAccByPlatform.Count() <= 0)
                    {
                        code = "error";
                        message = "Không có tài khoản Facebook nào";
                        goto lblResult;
                    }
                    for (int i = 0; i < facebook_percent; i++)
                    {
                        // Lấy ra tài khoản theo nền tảng đã chọn: Youtube/Google/Facebook -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
                        if (allAccByPlatform.Count <= 0) { allAccByPlatform = accUsed; }
                        accId = allAccByPlatform[0].accId;
                        accUsed.Add(allAccByPlatform[0]);
                        allAccByPlatform.RemoveAt(0);
                        // Lấy VPS -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
                        if (allVps.Count <= 0) { allVps = vpsUsed; }
                        vpsIp = allVps[0].vpsIp;
                        vpsUsed.Add(allVps[0]);
                        allVps.RemoveAt(0);
                        // Lấy UserAgent -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
                        if (allUa.Count <= 0) { allUa = uaUsed; }
                        uaId = allUa[0].uaId;
                        uaUsed.Add(allUa[0]);
                        allUa.RemoveAt(0);

                        // Tạo mới 1 Timeline Google
                        Timeline tl = new Timeline() { timelineName = string.Format("{0} {1} - {2}", "Auto Gen Facebook", count++, DateTime.Now.ToString("yyyyMMdd-") + Guid.NewGuid().ToString().Substring(0, 5)), timelineType = "facebook", timelineStatus = "deactive", userId = userId, accId = accId, uaId = uaId, vpsIp = vpsIp, timelineDelay = 0, timelineRepeat = tlRepeat, };
                        resultTL.Add(tl);
                        int newTlid = tl.UpdateOrInsert(tl);
                        tl.timelineId = newTlid;
                        //  fb_login mặc định
                        Actions fb_login = new Actions() { actName = "Login " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "fb_login", actTime = "0", actStatus = "active", userId = userId, };
                        actObject.UpdateOrInsert(fb_login);
                        resultAct.Add(fb_login);

                        //  fb_search mặc định
                        Actions fb_search = new Actions() { actName = "Search " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "fb_search", actTime = "0", actStatus = "active", userId = userId, actDesc = genKeyword, };
                        actObject.UpdateOrInsert(fb_search);
                        resultAct.Add(fb_search);

                        //  gg_back mặc định
                        Actions fb_back = new Actions() { actName = "Back " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "fb_back", actTime = "0", actStatus = "active", userId = userId, };
                        actObject.UpdateOrInsert(fb_back);
                        resultAct.Add(fb_back);
                    }
                }
                #endregion

                #region ================== Instagram ==================

                #endregion
                code = "success";
                message = "Tạo mới thành công";
            }
            catch (Exception ex)
            {
                code = "error";
                message = ex.Message;
                goto lblResult;
            }
        lblResult:
            //// Test 
            dict.Add("countTime", TimeSpan.FromSeconds(countTime));
            dict.Add("Timeline", resultTL);
            dict.Add("Timeline_count", resultTL.Count);
            dict.Add("Action", resultAct);
            dict.Add("Action_count", resultAct.Count);
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }
        #endregion ===================================== GenAuto =====================================


        //#region ===================================== GenAuto1 =====================================
        //public Dictionary<object, object> GenAuto1(List<string> genPlatform, List<string> genActions, float genAchived, int genTime, string genLink, string genKeyword, string genComment, int userId, string actMode)
        //{
        //    Dictionary<object, object> dict = new Dictionary<object, object>();
        //    string code = "error", message = "Thất bại";
        //    int maxAchived = 0;
        //    int accId = 0;
        //    string vpsIp = "";
        //    int uaId = 0;
        //    int actTime = 0;
        //    int countTime = 0;
        //    List<object> resultTL = new List<object>();
        //    List<object> resultAct = new List<object>();
        //    int count = 1;
        //    int tlRepeat = 0;
        //    int maxTime = 60;
        //    int minTime = 45; // Thời gian nhỏ nhất của 1 lần view

        //    // Tỷ lệ % like/dislike/comment
        //    float like_percent = (genAchived / 100) * 10;   //  100
        //    float dislike_percent = (genAchived / 100) * 5; //  50
        //    float comment_percent = (genAchived / 100) * 3; //  70
        //    bool check_to_view_only = false;
        //    // Biến lưu danh sách tài khoản và vps đã sử dụng lại để tránh lặp lại
        //    List<Account> accUsed = new List<Account>();
        //    List<Vps> vpsUsed = new List<Vps>();
        //    List<UserAgent> uaUsed = new List<UserAgent>();

        //    // Test
        //    //int newTlid = 0;
        //    int actId = 1;
        //    int count_like = (int)like_percent;
        //    int count_dislike = (int)dislike_percent;
        //    int count_comment = (int)comment_percent;

        //    // Tính % các platform (mặc định để 100% theo từng platform)
        //    float youtube_percent = (genAchived / 100) * 100;
        //    float google_percent = (genAchived / 100) * 100;
        //    float facebook_percent = (genAchived / 100) * 100;
        //    float instagram_percent = (genAchived / 100) * 100;

        //    if (genPlatform.Contains("youtube") && genPlatform.Contains("google"))
        //    {
        //        youtube_percent = (genAchived / 100) * 93;
        //        google_percent = (genAchived / 100) * 7;
        //    }
        //    if (genPlatform.Contains("youtube") && genPlatform.Contains("google") && genPlatform.Contains("facebook"))
        //    {
        //        youtube_percent = (genAchived / 100) * 83;
        //        google_percent = (genAchived / 100) * 7;
        //        facebook_percent = (genAchived / 100) * 10;
        //    }
        //    // Nếu chọn tất cả các platform
        //    if (genPlatform.Count() == 4)
        //    {
        //        youtube_percent = (genAchived / 100) * 73;
        //        google_percent = (genAchived / 100) * 7;
        //        facebook_percent = (genAchived / 100) * 10;
        //        instagram_percent = (genAchived / 100) * 10;
        //    }
        //    if (genPlatform.Contains("all"))
        //    {
        //        youtube_percent = (genAchived / 100) * 70;
        //        google_percent = (genAchived / 100) * 30;
        //        genPlatform.Add("youtube");
        //        genPlatform.Add("google");
        //    }
        //    try
        //    {

        //        List<Vps> allVps = new Vps().Get(0, "", "", "active", "", "");
        //        List<UserAgent> allUa = new UserAgent().Get(0, "", "active");
        //        Actions actObject = new Actions();

        //        /*=========================================== TÍNH TỶ LỆ PHẦN TRĂM LIKE - DISLIKE - COMMENT =========================================== */
        //        if (genPlatform.Contains("youtube"))
        //        {
        //            List<Account> allAccByPlatform = new Account().Get(0, "", "active", "youtube", "");
        //            List<Account> accVip = new List<Account>();
        //            foreach (var item in allAccByPlatform)
        //            {
        //                if (item.accNote.Contains("vip") || item.accNote == "VIP")
        //                {
        //                    accVip.Add(item);
        //                }
        //            }
        //            // Tạo mới Timeline + Actions cho 10% like
        //            // like_percent lúc này 20% của 1000 =======> 100 cái Timeline đầy đủ bao gồm 100 like + 70 comment
        //            // like_percent = 100
        //            // 70 cái comment

        //            #region ========================== Tạo mới 200 action yt_search và yt_view và yt_like mặc định ==========================
        //            for (int i = 0; i < (int)youtube_percent; i++)
        //            {
        //                // Lấy ra tài khoản theo nền tảng đã chọn: Youtube/Google/Facebook -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
        //                if (allAccByPlatform.Count <= 0) { allAccByPlatform = accUsed; }
        //                accId = allAccByPlatform[0].accId;
        //                accUsed.Add(allAccByPlatform[0]);
        //                allAccByPlatform.RemoveAt(0);
        //                // Lấy VPS -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
        //                if (allVps.Count <= 0) { allVps = vpsUsed; }
        //                vpsIp = allVps[0].vpsIp;
        //                vpsUsed.Add(allVps[0]);
        //                allVps.RemoveAt(0);
        //                // Lấy UserAgent -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
        //                if (allUa.Count <= 0) { allUa = uaUsed; }
        //                uaId = allUa[0].uaId;
        //                uaUsed.Add(allUa[0]);
        //                allUa.RemoveAt(0);

        //                // Random từ min 45s - 120s trở đi để Youtube có thể tính là 1 lượt view
        //                actTime = new Random().Next(minTime, maxTime);


        //                // Check khi chỉ có view thì sẽ không + thêm thời gian để test vì nó có repeat khác server, khác useragent, khác tài khoản rồi
        //                //if (check_to_view_only)
        //                //{
        //                //    actTime = 0;
        //                //}
        //                //else
        //                //{
        //                //    countTime += actTime;
        //                //}
        //                countTime += actTime;
        //                //newTlid += 1;
        //                // Tạo ra số chỉ có Actions yt_view còn lại
        //                //if (check_to_view_only)
        //                //{
        //                //    tlRepeat = new Random().Next(20, 30);
        //                //    genAchived -= tlRepeat;
        //                //    if (actTime > 0)
        //                //    {
        //                //        countTime *= tlRepeat;
        //                //    }
        //                //}
        //                if ((genActions.Contains("like") && i < like_percent) || (genActions.Contains("comment") && i < comment_percent))
        //                {
        //                    accId = accVip[new Random().Next(0, accVip.Count)].accId;
        //                }
        //                // Tạo mới 1 Timeline
        //                Timeline tl = new Timeline() { timelineName = string.Format("{0} {1} - {2}", "Auto Gen Youtube", count++, DateTime.Now.ToString("yyyyMMdd-") + Guid.NewGuid().ToString().Substring(0, 5)), timelineType = "youtube", timelineStatus = "deactive", userId = userId, accId = accId, uaId = uaId, vpsIp = vpsIp, timelineDelay = 0, timelineRepeat = 0, };
        //                resultTL.Add(tl);
        //                int newTlid = tl.UpdateOrInsert(tl);
        //                tl.timelineId = newTlid;
        //                if (genActions.Contains("search"))
        //                {
        //                    //  100 yt_search mặc định
        //                    Actions yt_search = new Actions() { actName = "Search " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_search", actTime = "0", actStatus = "active", userId = userId, actDesc = genKeyword, actComment = genComment, actMode = actMode, };
        //                    actObject.UpdateOrInsert(yt_search);
        //                    resultAct.Add(yt_search);
        //                }
        //                if (genActions.Contains("view"))
        //                {
        //                    // 100 yt_view mặc định
        //                    Actions yt_view = new Actions() { actName = "View " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_view", actTime = actTime.ToString(), actStatus = "active", userId = userId, actComment = genComment, actMode = actMode, actDesc = genKeyword, };
        //                    maxAchived += 1;
        //                    actObject.UpdateOrInsert(yt_view);
        //                    resultAct.Add(yt_view);
        //                }
        //                if (genActions.Contains("back"))
        //                {
        //                    // Thêm Actions yt_back của số lượng còn lại:  yt_search ---> yt_view ---> yt_back
        //                    if (check_to_view_only)
        //                    {
        //                        // Random yt_back
        //                        Actions act_back = new Actions() { actName = "Back " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_back", actTime = "0", actStatus = "active", userId = userId, };
        //                        resultAct.Add(act_back);
        //                        int newActId = actObject.UpdateOrInsert(act_back);
        //                    }
        //                }
        //                if (genActions.Contains("like"))
        //                {
        //                    // Chạy đến đây thì đã có 100 yt_search +  100 yt_view + 100 yt_login + 100 yt_like trong tổng 100 Timeline login_like đầu tiên
        //                    // 70 yt_comment
        //                    // Vì 10% của 1000 = 100 cái Actions yt_login + 100 yt_like
        //                    if (i < like_percent)
        //                    {
        //                        // 100 yt_login mặc định
        //                        Actions act_login = new Actions() { actName = "Login " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_login", actTime = "0", actStatus = "active", userId = userId, };
        //                        resultAct.Add(act_login);
        //                        int newActId = actObject.UpdateOrInsert(act_login);

        //                        // 100 yt_like
        //                        Actions yt_like = new Actions() { actName = "Like " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_like", actTime = "0", actStatus = "active", userId = userId, actComment = genComment, actMode = actMode, actDesc = genKeyword, };
        //                        maxAchived += 1;
        //                        resultAct.Add(yt_like);
        //                        actObject.UpdateOrInsert(yt_like);
        //                        count_like--;
        //                    }
        //                }
        //                if (genActions.Contains("comment"))
        //                {
        //                    // Chạy đến đây thì đã có 100 yt_search +  100 yt_view + 100 yt_login + 100 yt_like trong tổng 100 Timeline login_like đầu tiên
        //                    // 70 yt_comment
        //                    // Vì 7% của 1000 = 70 cái Actions yt_comment
        //                    if (i < comment_percent)
        //                    {
        //                        Actions yt_comment = new Actions() { actName = "Comment " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_comment", actTime = "0", actStatus = "active", userId = userId, actComment = genComment, actMode = actMode, actDesc = genKeyword, };
        //                        maxAchived += 1;
        //                        resultAct.Add(yt_comment);
        //                        int new_act_comment = actObject.UpdateOrInsert(yt_comment);
        //                        count_comment--;
        //                        // Random yt_back
        //                        Actions act_back = new Actions() { actName = "Back " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_back", actTime = "0", actStatus = "active", userId = userId, };
        //                        resultAct.Add(act_back);
        //                        int newActId = actObject.UpdateOrInsert(act_back);
        //                        if (count_comment <= 0)
        //                        {
        //                            continue;
        //                        }
        //                    }
        //                }
        //                #endregion
        //                if (genActions.Contains("dislike"))
        //                {
        //                    // Tạo ra 50 cái yt_dislike
        //                    if (count_like <= 0 && count_comment <= 0 && count_dislike > 0)
        //                    {
        //                        // 100 yt_login mặc định
        //                        Actions act_login = new Actions() { actName = "Login " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_login", actTime = "0", actStatus = "active", userId = userId, };
        //                        resultAct.Add(act_login);
        //                        actObject.UpdateOrInsert(act_login);
        //                        // Tạo mới 1 action dislike đối thủ trước khi vào like của mình
        //                        Actions yt_dislike = new Actions() { actName = "Dislike " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_dislike", actTime = "0", actStatus = "active", userId = userId, actComment = genComment, actMode = actMode, actDesc = genKeyword, };
        //                        maxAchived += 1;
        //                        resultAct.Add(yt_dislike);
        //                        actObject.UpdateOrInsert(yt_dislike);
        //                        count_dislike--;
        //                        if (count_dislike <= 0)
        //                        {
        //                            check_to_view_only = true;
        //                        }
        //                        // Random yt_back
        //                        Actions act_back = new Actions() { actName = "Back " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "yt_back", actTime = "0", actStatus = "active", userId = userId, };
        //                        resultAct.Add(act_back);
        //                        int newActId = actObject.UpdateOrInsert(act_back);
        //                    }
        //                }
        //            }
        //        }
        //        #region ================== Google ==================
        //        if (genPlatform.Contains("google"))
        //        {
        //            List<Account> allAccByPlatform = new Account().Get(0, "", "active", "youtube", "");
        //            if (allAccByPlatform.Count() <= 0)
        //            {
        //                code = "error";
        //                message = "Không có tài khoản Google nào";
        //                goto lblResult;
        //            }
        //            for (int i = 0; i < google_percent; i++)
        //            {
        //                // Lấy ra tài khoản theo nền tảng đã chọn: Youtube/Google/Facebook -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
        //                if (allAccByPlatform.Count <= 0) { allAccByPlatform = accUsed; }
        //                accId = allAccByPlatform[0].accId;
        //                accUsed.Add(allAccByPlatform[0]);
        //                allAccByPlatform.RemoveAt(0);
        //                // Lấy VPS -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
        //                if (allVps.Count <= 0) { allVps = vpsUsed; }
        //                vpsIp = allVps[0].vpsIp;
        //                vpsUsed.Add(allVps[0]);
        //                allVps.RemoveAt(0);
        //                // Lấy UserAgent -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
        //                if (allUa.Count <= 0) { allUa = uaUsed; }
        //                uaId = allUa[0].uaId;
        //                uaUsed.Add(allUa[0]);
        //                allUa.RemoveAt(0);

        //                // Tạo mới 1 Timeline Google
        //                Timeline tl = new Timeline() { timelineName = string.Format("{0} {1} - {2}", "Auto Gen Google", count++, DateTime.Now.ToString("yyyyMMdd-") + Guid.NewGuid().ToString().Substring(0, 5)), timelineType = "google", timelineStatus = "deactive", userId = userId, accId = accId, uaId = uaId, vpsIp = vpsIp, timelineDelay = 0, timelineRepeat = 0, };
        //                resultTL.Add(tl);
        //                int newTlid = tl.UpdateOrInsert(tl);
        //                tl.timelineId = newTlid;
        //                //  100 gg_search mặc định
        //                Actions gg_search = new Actions() { actName = "Search " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "gg_search", actTime = "0", actStatus = "active", userId = userId, actDesc = genKeyword, actComment = genComment, actMode = actMode, };
        //                actObject.UpdateOrInsert(gg_search);
        //                resultAct.Add(gg_search);

        //                //  100 gg_back mặc định
        //                Actions gg_back = new Actions() { actName = "Back " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "gg_back", actTime = "0", actStatus = "active", userId = userId, actDesc = genKeyword, actComment = genComment, actMode = actMode, };
        //                actObject.UpdateOrInsert(gg_back);
        //                resultAct.Add(gg_back);
        //            }
        //        }
        //        #endregion

        //        #region ================== Facebook ==================
        //        if (genPlatform.Contains("facebook"))
        //        {
        //            List<Account> allAccByPlatform = new Account().Get(0, "", "active", "facebook", "");

        //            if (allAccByPlatform.Count() <= 0)
        //            {
        //                code = "error";
        //                message = "Không có tài khoản Facebook nào";
        //                goto lblResult;
        //            }
        //            for (int i = 0; i < facebook_percent; i++)
        //            {
        //                // Lấy ra tài khoản theo nền tảng đã chọn: Youtube/Google/Facebook -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
        //                if (allAccByPlatform.Count <= 0) { allAccByPlatform = accUsed; }
        //                accId = allAccByPlatform[0].accId;
        //                accUsed.Add(allAccByPlatform[0]);
        //                allAccByPlatform.RemoveAt(0);
        //                // Lấy VPS -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
        //                if (allVps.Count <= 0) { allVps = vpsUsed; }
        //                vpsIp = allVps[0].vpsIp;
        //                vpsUsed.Add(allVps[0]);
        //                allVps.RemoveAt(0);
        //                // Lấy UserAgent -----------> xong sẽ xóa khỏi danh sách đó vì đã dùng rồi
        //                if (allUa.Count <= 0) { allUa = uaUsed; }
        //                uaId = allUa[0].uaId;
        //                uaUsed.Add(allUa[0]);
        //                allUa.RemoveAt(0);

        //                // Tạo mới 1 Timeline Google
        //                Timeline tl = new Timeline() { timelineName = string.Format("{0} {1} - {2}", "Auto Gen Facebook", count++, DateTime.Now.ToString("yyyyMMdd-") + Guid.NewGuid().ToString().Substring(0, 5)), timelineType = "facebook", timelineStatus = "deactive", userId = userId, accId = accId, uaId = uaId, vpsIp = vpsIp, timelineDelay = 0, timelineRepeat = 0, };
        //                resultTL.Add(tl);
        //                int newTlid = tl.UpdateOrInsert(tl);
        //                tl.timelineId = newTlid;
        //                //  fb_login mặc định
        //                Actions fb_login = new Actions() { actName = "Login " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "fb_login", actTime = "0", actStatus = "active", userId = userId, actDesc = genKeyword, };
        //                actObject.UpdateOrInsert(fb_login);
        //                resultAct.Add(fb_login);

        //                //  fb_search mặc định
        //                Actions fb_search = new Actions() { actName = "Search " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "fb_search", actTime = "0", actStatus = "active", userId = userId, actDesc = genKeyword, };
        //                actObject.UpdateOrInsert(fb_search);
        //                resultAct.Add(fb_search);

        //                //  gg_back mặc định
        //                Actions fb_back = new Actions() { actName = "Back " + tl.timelineId, timelineId = tl.timelineId, actLink = genLink, actType = "fb_back", actTime = "0", actStatus = "active", userId = userId, actDesc = genKeyword, };
        //                actObject.UpdateOrInsert(fb_back);
        //                resultAct.Add(fb_back);
        //            }
        //        }
        //        #endregion

        //        #region ================== Instagram ==================

        //        #endregion
        //        code = "success";
        //        message = "Tạo mới thành công";
        //    }
        //    catch (Exception ex)
        //    {
        //        code = "error";
        //        message = ex.Message;
        //        goto lblResult;
        //    }
        //lblResult:
        //    //// Test 
        //    dict.Add("countTime", TimeSpan.FromSeconds(countTime));
        //    dict.Add("Timeline", resultTL);
        //    dict.Add("Timeline_count", resultTL.Count);
        //    dict.Add("Action", resultAct);
        //    dict.Add("Action_count", resultAct.Count);
        //    dict.Add("code", code);
        //    dict.Add("message", message);
        //    return dict;
        //}
        //#endregion ===================================== GenAuto =====================================


    }
}