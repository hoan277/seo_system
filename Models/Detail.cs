﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class vw_Detail
    {
        public int detailId { get; set; }
        public string detailIp { get; set; }
        public string detailType { get; set; }
        public string detailCode { get; set; }
        public DateTime detailCreated { get; set; }
        public DateTime detailUpdated { get; set; }
        public string logCode { get; set; }
        public int timelineId { get; set; }
        public int actId { get; set; }
        public int accId { get; set; }
        public int uaId { get; set; }
        public string timelineName { get; set; }
        public string actName { get; set; }
        public string actTime { get; set; }
        public string actLink { get; set; }
        public string actType { get; set; }
        public string accName { get; set; }
        public string uaDesc { get; set; }
    }

    public class Detail
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int detailId { get; set; }
        public string detailIp { get; set; }
        public string detailType { get; set; }
        public string detailCode { get; set; }
        public DateTime detailCreated { get; set; }
        public DateTime detailUpdated { get; set; }
        public string logCode { get; set; }
        public int timelineId { get; set; }
        public int actId { get; set; }
        public int accId { get; set; }
        public int uaId { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ DANH MỤC =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Danh mục</summary>
        /// <returns>Trả về List Detail</returns>
        public List<Detail> GetAllDetail()
        {
            List<Detail> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Detail>();
                query.OrderByDescending(x => x.detailId);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Detail> GetByLogCode(string logCode)
        {
            List<Detail> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Detail>().Where(e => e.logCode == logCode);
                query.OrderByDescending(x => x.detailId);
                rows = db.Select(query).ToList();
            }
            return rows;
        }

        public List<vw_Detail> GetByLogCodeView(string logCode)
        {
            List<vw_Detail> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<vw_Detail>().Where(e => e.logCode == logCode);
                query.OrderByDescending(x => x.detailId);
                rows = db.Select(query).ToList();
            }
            return rows;
        }

        public List<Detail> List(int _limit, int _offset, string _search)
        {
            List<Detail> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Detail>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.detailIp == _search || e.detailId.ToString() == search);
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.detailId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ DANH MỤC =====================================

        #region ===================================== XÓA THÔNG QUA detailId=====================================

        /// <summary>Xóa 1 danh mục qua detailId</summary>
        /// <param name="detailId">  Mã detailId của danh mục muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int detailId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Detail>().Where(x => x.detailId == detailId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA detailId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Detail</summary>
        /// <param name="detail">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Detail detail)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (detail.detailId > 0)
                {
                    var queryUpdate = db.From<Detail>().Where(e => e.detailId == detail.detailId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.detailId = detail.detailId;
                        objUpdate.detailIp = detail.detailIp;
                        objUpdate.detailType = detail.detailType;
                        objUpdate.detailCode = detail.detailCode;
                        objUpdate.detailCreated = detail.detailCreated;
                        objUpdate.detailUpdated = DateTime.Now;
                        objUpdate.logCode = detail.logCode;
                        objUpdate.timelineId = detail.timelineId;
                        objUpdate.actId = detail.actId;
                        objUpdate.accId = detail.accId;
                        objUpdate.uaId = detail.uaId;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Detail
                    {
                        detailIp = detail.detailIp,
                        detailType = detail.detailType,
                        detailCode = detail.detailCode,
                        detailCreated = DateTime.Now,
                        detailUpdated = detail.detailUpdated,
                        logCode = DateTime.Now.ToString("dd-MM-yyyy"),
                        timelineId = detail.timelineId,
                        actId = detail.actId,
                        accId = detail.accId,
                        uaId = detail.uaId,
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        public Dictionary<object, object> UpdateTimeEnd(int detailId)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "Thất bại", code = "error";
            using (var db = DatabaseUtils.OpenConnection())
            {
                if (detailId > 0)
                {
                    var queryUpdate = db.From<Detail>().Where(e => e.detailId == detailId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.detailUpdated = DateTime.Now;
                        if (db.Update(objUpdate) > 0)
                        {
                            code = "success";
                            message = "Thành công";
                        }
                    }
                }
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        #region ===================================== Batch Delete =====================================

        public Dictionary<object, object> BatchDelete(int[] list_selected)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "error", code = "Chưa có item nào được chọn để xóa";
            if (list_selected.Length > 0)
            {
                for (int i = 0; i < list_selected.Length; i++)
                {
                    Delete(list_selected[i]);
                }
                code = "success";
                message = "Xóa thành công";
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        #endregion ===================================== Batch Delete =====================================
    }
}