﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Timeline
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int timelineId { get; set; }
        public string timelineName { get; set; }
        public string timelineType { get; set; }
        public DateTime timelineCreated { get; set; }
        public DateTime timelineUpdated { get; set; }
        public string timelineStatus { get; set; }
        public int userId { get; set; }
        public int accId { get; set; }
        public int uaId { get; set; }
        public string vpsIp { get; set; }
        public int timelineDelay { get; set; }
        public int timelineRepeat { get; set; }
        public int timelineRepeatTemp { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ Timeline =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Timeline</summary>
        /// <returns>Trả về List Timeline</returns>
        public List<Timeline> GetAllTimeline()
        {
            List<Timeline> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Timeline>();
                query.OrderByDescending(x => x.timelineId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ Timeline =====================================

        #region ===================================== Get =====================================

        public List<Timeline> Get(int timelineId, string timelineName, string timelineStatus, string timelineType)
        {
            List<Timeline> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Timeline>();
                if (timelineId > 0) { query = query.Where(e => e.timelineId == timelineId); }
                if (!string.IsNullOrEmpty(timelineName)) { query = query.Where(e => e.timelineName == timelineName || e.timelineName.Contains(timelineName)); }
                if (!string.IsNullOrEmpty(timelineStatus)) { query = query.Where(e => e.timelineStatus == timelineStatus); }
                if (!string.IsNullOrEmpty(timelineType)) { query = query.Where(e => e.timelineType == timelineType); }
                query.OrderByRandom();
                rows = db.Select(query).ToList();
            }
            return rows;
        }

        #endregion ===================================== Get =====================================

        #region ===================================== GetTimelineAction =====================================

        public Dictionary<object, object> GetTimelineAction(int timelineId, string timelineStatus, string timelineType, string vpsIp, string actMode)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            Timeline timeline = new Timeline();
            string message = "Không có dữ liệu", code = "error";
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Timeline>();
                if (timelineId > 0) { query = query.Where(e => e.timelineId == timelineId); }
                if (!string.IsNullOrEmpty(timelineStatus)) { query = query.Where(e => e.timelineStatus == timelineStatus); }
                if (!string.IsNullOrEmpty(timelineType)) { query = query.Where(e => e.timelineType == timelineType); }
                if (!string.IsNullOrEmpty(vpsIp)) { query = query.Where(e => e.vpsIp == vpsIp); }
                query.OrderByRandom();
                timeline = db.Select(query).LastOrDefault();
                if (timeline != null)
                {
                    dict.Add("Timeline", timeline);
                    Account acc = new Account().GetAccountById(timeline.accId);
                    if (acc != null)
                    {
                        dict.Add("Account", acc);
                    }
                    UserAgent list_useragent = new UserAgent().GetById(timeline.uaId);
                    if (list_useragent != null)
                    {
                        dict.Add("UserAgent", list_useragent);
                    }
                    List<Actions> list_action = new Actions().GetActionByTimelineId(timeline.timelineId, actMode);
                    if (list_action != null)
                    {
                        dict.Add("Actions", list_action);
                    }

                    code = "success";
                    message = "Lấy dữ liệu thành công";
                }
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        public Dictionary<object, object> BatchDuplicate(int[] list_selected, string[] duplicate_options)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string code = "error", message = "Thất bại";
            using (var db = DatabaseUtils.OpenConnection())
            {
                for (int i = 0; i < list_selected.Length; i++)
                {
                    var query = db.From<Timeline>().Where(e => e.timelineId == list_selected[i]);
                    var obj = db.Select(query).LastOrDefault();
                    if (obj != null)
                    {
                        Timeline newTimeline = obj;
                        newTimeline.timelineId = 0;
                        newTimeline.timelineName = obj.timelineName + " - Copy";
                        newTimeline.timelineStatus = "deactive";
                        int newTimelineId = UpdateOrInsert(newTimeline);

                        if (duplicate_options.Length > 1)
                        {
                            var queryActions = db.From<Actions>().Where(e => e.timelineId == list_selected[i]);
                            var objActions = db.Select(queryActions).ToList();
                            if (objActions != null)
                            {
                                foreach (var act in objActions)
                                {
                                    Actions newAct = act;
                                    newAct.actId = 0;
                                    newAct.timelineId = newTimelineId;
                                    new Actions().UpdateOrInsert(newAct);
                                }
                            }
                        }
                        code = "success";
                        message = "Nhân bản thành công";
                    }
                }
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        public Dictionary<object, object> UpdateStatus(int timelineId, string timelineStatus)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "Cập nhật trạng thái lỗi", code = "error";
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Timeline>().Where(e => e.timelineId == timelineId);
                var obj = db.Select(query).LastOrDefault();
                List<Account> allAccByPlatform = new Account().Get(0, "", "active", obj.timelineType, "");
                string vpsGroup = "";
                try
                {
                    List<Vps> lstVPS = new Vps().Get(0, "", obj.vpsIp, "", "", "", "");
                    if (lstVPS.Count > 0)
                    {
                        vpsGroup = lstVPS[0].vpsGroup;
                    }
                }
                catch (Exception)
                {
                    vpsGroup = "";
                }
                List<Vps> allVps = new Vps().Get(0, "", "", "active", "", "", vpsGroup);
                List<UserAgent> allUa = new UserAgent().Get(0, "", "active");
                if (obj != null)
                {
                    if (timelineStatus == "done")
                    {
                        if (obj.timelineRepeatTemp < obj.timelineRepeat)
                        {
                            // Đổi IP khác, Tài khoản khác, UserAgent khác
                            if (allVps.Count > 0)
                            {
                                vpsIp = allVps[new Random().Next(0, allVps.Count)].vpsIp;
                            }
                            if (allUa.Count > 0)
                            {
                                uaId = allUa[new Random().Next(0, allUa.Count)].uaId;
                            }
                            if (allAccByPlatform.Count > 0)
                            {
                                accId = allAccByPlatform[new Random().Next(0, allAccByPlatform.Count)].accId;
                            }
                            timelineStatus = "active";
                            obj.timelineRepeatTemp += 1;
                        }
                        if (obj.timelineRepeatTemp == obj.timelineRepeat)
                        {
                            timelineStatus = "done";
                        }
                    }
                    obj.timelineStatus = timelineStatus;
                    obj.timelineUpdated = DateTime.Now;
                    try
                    {
                        db.Update(obj);
                        code = "success";
                        message = "Đổi trạng thái thành công lần " + obj.timelineRepeatTemp;
                    }
                    catch (Exception ex)
                    {
                        code = "error";
                        message = ex.Message;
                    }
                }
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        public Dictionary<object, object> BatchUpdateStatus(int[] list_selected, string status)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string code = "error", message = "Thất bại";
            using (var db = DatabaseUtils.OpenConnection())
            {
                for (int i = 0; i < list_selected.Length; i++)
                {
                    var query = db.From<Timeline>().Where(e => e.timelineId == list_selected[i]);
                    var obj = db.Select(query).LastOrDefault();
                    if (obj != null)
                    {
                        obj.timelineStatus = status;
                        try
                        {
                            UpdateOrInsert(obj);
                            code = "success";
                            message = "Cập nhật trạng thái thành công";
                        }
                        catch (Exception ex)
                        {
                            code = "error";
                            message = ex.Message;
                            goto lblDone;
                        }
                    }
                }
            }
        lblDone:
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        #endregion ===================================== GetTimelineAction =====================================

        #region ===================================== GetByTimelineId =====================================

        public Timeline GetByTimelineId(int timelineId)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Timeline>();
                if (timelineId > 0) { query = query.Where(e => e.timelineId == timelineId); }
                return db.Select(query).LastOrDefault();
            }
        }

        #endregion ===================================== GetByTimelineId =====================================

        #region ===================================== Batch Delete =====================================

        public Dictionary<object, object> BatchDelete(int[] list_selected)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "error", code = "Chưa có item nào được chọn để xóa";
            if (list_selected.Length > 0)
            {
                for (int i = 0; i < list_selected.Length; i++)
                {
                    Delete(list_selected[i]);
                }
                code = "success";
                message = "Xóa thành công";
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        #endregion ===================================== Batch Delete =====================================

        #region ===================================== SearchTimeline =====================================

        public List<Timeline> SearchTimeline(int _limit, int _offset, string _search)
        {
            List<Timeline> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Timeline>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.timelineName == search || e.timelineName.Contains(search) || e.timelineId.ToString() == search
                || e.timelineType == search || e.timelineType.Contains(search)
                || e.timelineStatus == search || e.timelineStatus.Contains(search)
                );
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.timelineId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== SearchTimeline =====================================

        #region ===================================== GetById =====================================
        public Timeline GetById(int timelineId)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Timeline>().Where(e => e.timelineId == timelineId);
                return db.Select(query).SingleOrDefault();
            }
        }
        #endregion ===================================== GetById =====================================

        #region ===================================== XÓA THÔNG QUA timelineId=====================================

        /// <summary>Xóa 1 Timeline qua timelineId</summary>
        /// <param name="timelineId">  Mã timelineId của Timeline muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int timelineId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // Xóa hết Actions của Timeline này đi
                var queryAction = db.From<Actions>().Where(x => x.timelineId == timelineId);
                if (db.Select(queryAction).Count > 0)
                {
                    db.Delete(queryAction);
                }
                // Xóa Timeline
                var queryDelete = db.From<Timeline>().Where(x => x.timelineId == timelineId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA timelineId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Timeline</summary>
        /// <param name="timeline">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Timeline timeline)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (timeline.timelineId > 0)
                {
                    var queryUpdate = db.From<Timeline>().Where(e => e.timelineId == timeline.timelineId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.timelineId = timeline.timelineId;
                        objUpdate.timelineName = timeline.timelineName;
                        objUpdate.timelineType = timeline.timelineType;
                        objUpdate.timelineCreated = timeline.timelineCreated;
                        objUpdate.timelineUpdated = DateTime.Now;
                        objUpdate.timelineStatus = timeline.timelineStatus;
                        objUpdate.userId = timeline.userId;
                        objUpdate.accId = timeline.accId;
                        objUpdate.uaId = timeline.uaId;
                        objUpdate.vpsIp = timeline.vpsIp;
                        objUpdate.timelineDelay = timeline.timelineDelay;
                        objUpdate.timelineRepeat = timeline.timelineRepeat;
                        objUpdate.timelineRepeatTemp = timeline.timelineRepeatTemp > 0 ? timeline.timelineRepeatTemp : 0;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Timeline()
                    {
                        timelineName = timeline.timelineName,
                        timelineType = timeline.timelineType,
                        timelineCreated = DateTime.Now,
                        timelineUpdated = DateTime.Now,
                        timelineStatus = timeline.timelineStatus,
                        userId = timeline.userId,
                        accId = timeline.accId,
                        uaId = timeline.uaId,
                        vpsIp = timeline.vpsIp,
                        timelineDelay = timeline.timelineDelay,
                        timelineRepeat = timeline.timelineRepeat,
                        timelineRepeatTemp = timeline.timelineRepeatTemp,
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
    }
}