﻿using Microsoft.Extensions.Configuration;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Data;
using System.IO;
using System.Reflection;

namespace SEO_System.Models
{
    public class DatabaseUtils
    {
        public static OrmLiteConnectionFactory cnn;

        /// <summary>Khởi tạo một thể hiện mới và đọc chuỗi kết nối  <see cref="DatabaseUtils"/> class.</summary>
        public DatabaseUtils()
        {
            OrmLiteConfig.DialectProvider = MySqlDialect.Provider;
            bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";
            cnn = new OrmLiteConnectionFactory(GetConnectionString(isDev ? "server" : "local"), OrmLiteConfig.DialectProvider);
        }

        public static IConfigurationRoot Configuration;

        public static string GetConnectionString(string server)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            return Configuration[server];
        }

        /// <summary>
        ///   <para>Mở kết nối đến cơ sở dữ liệu</para>
        /// </summary>
        /// <returns>Trả về một IDbConenction</returns>
        public static IDbConnection OpenConnection()
        {
            DatabaseUtils du = new DatabaseUtils();
            IDbConnection dbConnection = cnn.OpenDbConnection();
            if (dbConnection.State == ConnectionState.Closed)
            {
                dbConnection.Open();
            }
            //else {
            //    dbConnection.Close();
            //}
            return dbConnection;
        }
        public static void SetLicense()
        {
            var licenseUtils = typeof(LicenseUtils);
            var members = licenseUtils.FindMembers(MemberTypes.All, BindingFlags.NonPublic | BindingFlags.Static, null, null);
            Type activatedLicenseType = null;
            foreach (var memberInfo in members)
            {
                if (memberInfo.Name.Equals("__activatedLicense", StringComparison.OrdinalIgnoreCase) && memberInfo is FieldInfo fieldInfo)
                    activatedLicenseType = fieldInfo.FieldType;
            }

            if (activatedLicenseType != null)
            {
                var licenseKey = new LicenseKey
                {
                    Expiry = DateTime.Now.AddYears(100),
                    Ref = "ServiceStack",
                    Name = "Enterprise",
                    Type = LicenseType.Enterprise
                };
                var constructor = activatedLicenseType.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { typeof(LicenseKey) }, null);
                if (constructor != null)
                {
                    var activatedLicense = constructor.Invoke(new object[] { licenseKey });
                    var activatedLicenseField = licenseUtils.GetField("__activatedLicense", BindingFlags.NonPublic | BindingFlags.Static);
                    if (activatedLicenseField != null)
                        activatedLicenseField.SetValue(null, activatedLicense);
                }
            }
        }
    }
}