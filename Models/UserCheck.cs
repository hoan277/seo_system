﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class UserCheck
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int Id { get; set; }
        public string code { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string email_recovery { get; set; }
        public string youtube_channel { get; set; }
        public string youtube_channel_name { get; set; }
        public DateTime datecreated_email { get; set; }
        public int login_status { get; set; }
        public int verification_status { get; set; }
        public int status { get; set; }
        public int checkin_24 { get; set; }
        public int checkin_181 { get; set; }
        public int checkin_182 { get; set; }
        public DateTime datecreated { get; set; }
        public DateTime dateupdated { get; set; }
        public string new_email_recovery { get; set; }
        public string cookies { get; set; }
        public int recovery_change_status { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ UserCheck =====================================
        public List<UserCheck> GetAllUserCheck()
        {
            List<UserCheck> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<UserCheck>();
                query.OrderByDescending(x => x.Id);
                rows = db.Select(query);
            }
            return rows;
        }
        public List<UserCheck> GetAllUserByStatus(string type, int status)
        {
            List<UserCheck> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<UserCheck>().OrderByDescending(x => x.Id);
                if (type == "login")
                {
                    query = query.Where(e => e.login_status == status);
                }
                if (type == "status")
                {
                    query = query.Where(e => e.status == status);
                }
                if (type == "verification")
                {
                    query = query.Where(e => e.verification_status == status);
                }
                if (type == "checkin_24")
                {
                    query = query.Where(e => e.checkin_24 == status);
                }
                if (type == "checkin_181")
                {
                    query = query.Where(e => e.checkin_181 == status);
                }
                if (type == "checkin_182")
                {
                    query = query.Where(e => e.checkin_182 == status);
                }
                if (type == "recovery_change_status")
                {
                    query = query.Where(e => e.recovery_change_status == status);
                }
                rows = db.Select(query);
            }
            return rows;
        }
        public int UpdateStatus(string type, string email, int status)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryUpdate = db.From<UserCheck>().Where(e => e.email == email);
                var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                if (objUpdate != null)
                {
                    if (type == "login")
                    {
                        objUpdate.login_status = status;
                    }
                    if (type == "status")
                    {
                        objUpdate.status = status;
                    }
                    if (type == "verification")
                    {
                        objUpdate.verification_status = status;
                    }
                    if (type == "checkin_24")
                    {
                        objUpdate.checkin_24 = status;
                    }
                    if (type == "checkin_181")
                    {
                        objUpdate.checkin_181 = status;
                    }
                    if (type == "checkin_182")
                    {
                        objUpdate.checkin_182 = status;
                    }
                    if (type == "recovery_change_status")
                    {
                        objUpdate.recovery_change_status = status;
                    }
                    objUpdate.dateupdated = DateTime.Now;
                    return db.Update(objUpdate);
                }
                return -1;
            }
        }

        public int UpdateCookies(string email, string cookies)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryUpdate = db.From<UserCheck>().Where(e => e.email == email);
                var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.cookies = cookies;
                    objUpdate.dateupdated = DateTime.Now;
                    return db.Update(objUpdate);
                }
                return -1;
            }
        }

        #endregion ===================================== LẤY TẤT CẢ UserCheck =====================================

        #region ===================================== XÓA THÔNG QUA Id=====================================
        public bool Delete(int Id)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<UserCheck>().Where(x => x.Id == Id);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }
        #endregion ===================================== XÓA THÔNG QUA Id=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
        public int UpdateOrInsert(UserCheck ua)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                if (ua.Id > 0)
                {
                    var queryUpdate = db.From<UserCheck>().Where(e => e.Id == ua.Id);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.Id = ua.Id;
                        objUpdate.code = ua.code;
                        objUpdate.email = ua.email;
                        objUpdate.password = ua.password;
                        objUpdate.email_recovery = ua.email_recovery;
                        objUpdate.youtube_channel = ua.youtube_channel;
                        objUpdate.youtube_channel_name = ua.youtube_channel_name;
                        objUpdate.datecreated_email = ua.datecreated_email;
                        objUpdate.login_status = ua.login_status;
                        objUpdate.verification_status = ua.verification_status;
                        objUpdate.status = ua.status;
                        objUpdate.dateupdated = DateTime.Now;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new UserCheck
                    {
                        code = ua.code,
                        email = ua.email,
                        password = ua.password,
                        email_recovery = ua.email_recovery,
                        youtube_channel = ua.youtube_channel,
                        youtube_channel_name = ua.youtube_channel_name,
                        datecreated_email = ua.datecreated_email,
                        login_status = ua.login_status,
                        verification_status = ua.verification_status,
                        status = ua.status,
                        datecreated = DateTime.Now,
                        dateupdated = DateTime.Now
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                };
            }
            return result;
        }
        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
    }
}