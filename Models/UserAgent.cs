﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class UserAgent
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int uaId { get; set; }
        public string uaBrowser { get; set; }
        public string uaDesc { get; set; }
        public DateTime uaCreated { get; set; }
        public DateTime uaUpdated { get; set; }
        public string uaStatus { get; set; }
        public int userId { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ UserAgent =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Danh mục</summary>
        /// <returns>Trả về List UserAgent</returns>
        public List<UserAgent> GetAllUserAgent()
        {
            List<UserAgent> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<UserAgent>();
                query.OrderByDescending(x => x.uaId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ UserAgent =====================================

        #region ===================================== XÓA THÔNG QUA uaId=====================================

        /// <summary>Xóa 1 danh mục qua uaId</summary>
        /// <param name="uaId">  Mã uaId của danh mục muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int uaId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<UserAgent>().Where(x => x.uaId == uaId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA uaId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng UserAgent</summary>
        /// <param name="ua">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(UserAgent ua)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (ua.uaId > 0)
                {
                    var queryUpdate = db.From<UserAgent>().Where(e => e.uaId == ua.uaId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.uaId = ua.uaId;
                        objUpdate.uaBrowser = ua.uaBrowser;
                        objUpdate.uaDesc = ua.uaDesc;
                        objUpdate.uaCreated = ua.uaCreated;
                        objUpdate.uaUpdated = ua.uaUpdated;
                        objUpdate.uaStatus = ua.uaStatus;
                        objUpdate.userId = ua.userId;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new UserAgent
                    {
                        uaBrowser = ua.uaBrowser,
                        uaDesc = ua.uaDesc,
                        uaCreated = DateTime.Now,
                        uaUpdated = DateTime.Now,
                        uaStatus = ua.uaStatus,
                        userId = ua.userId,
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        #region ===================================== Get =====================================

        public List<UserAgent> Get(int uaId, string uaDesc, string uaStatus)
        {
            List<UserAgent> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<UserAgent>();
                if (uaId > 0) { query = query.Where(e => e.uaId == uaId); }
                if (!string.IsNullOrEmpty(uaDesc)) { query = query.Where(e => e.uaDesc == uaDesc); }
                if (!string.IsNullOrEmpty(uaStatus)) { query = query.Where(e => e.uaStatus == uaStatus); }
                query.OrderByDescending(x => x.uaId);
                rows = db.Select(query).ToList();
            }
            return rows;
        }

        #endregion ===================================== Get =====================================

        #region ===================================== Batch Delete =====================================

        public Dictionary<object, object> BatchDelete(int[] list_selected)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "error", code = "Chưa có item nào được chọn để xóa";
            if (list_selected.Length > 0)
            {
                for (int i = 0; i < list_selected.Length; i++)
                {
                    Delete(list_selected[i]);
                }
                code = "success";
                message = "Xóa thành công";
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        #endregion ===================================== Batch Delete =====================================

        #region ===================================== SearchUserAgent =====================================

        public List<UserAgent> SearchUserAgent(int _limit, int _offset, string _search)
        {
            List<UserAgent> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<UserAgent>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.uaDesc == search || e.uaDesc.Contains(search) || e.uaId.ToString() == search);
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.uaId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== SearchUserAgent =====================================
        #region ===================================== GetById =====================================

        public UserAgent GetById(int uaId)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<UserAgent>().Where(e => e.uaId == uaId);
                return db.Select(query).LastOrDefault();
            }
        }
        #endregion ===================================== GetById =====================================

        #region ===================================== BatchUpdateStatus =====================================
        public Dictionary<object, object> BatchUpdateStatus(int[] list_selected, string status)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string code = "error", message = "Thất bại";
            using (var db = DatabaseUtils.OpenConnection())
            {
                for (int i = 0; i < list_selected.Length; i++)
                {
                    var query = db.From<UserAgent>().Where(e => e.uaId == list_selected[i]);
                    var obj = db.Select(query).LastOrDefault();
                    if (obj != null)
                    {
                        obj.uaStatus = status;
                        try
                        {
                            UpdateOrInsert(obj);
                            code = "success";
                            message = "Cập nhật trạng thái thành công";
                        }
                        catch (Exception ex)
                        {
                            code = "error";
                            message = ex.Message;
                            goto lblDone;
                        }
                    }
                }
            }
        lblDone:
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }
        #endregion ===================================== BatchUpdateStatus =====================================
    }
}