﻿using System.Collections.Generic;

namespace SEO_System.Models
{
    public class Home
    {
        public Dictionary<object, object> CountAll()
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "Thất bại", code = "error";
            try
            {
                int timelime = new Timeline().GetAllTimeline().Count;
                int actions = new Actions().GetAllActions().Count;
                int account = new Account().GetAllAccount().Count;
                int link = new Link().GetAllLink().Count;
                int comment = new Comment().GetAllComment().Count;
                int user = new User().GetAllUser().Count;
                int channel = new Channel().GetAllChannel().Count;
                int useragent = new UserAgent().GetAllUserAgent().Count;
                int model = new Model().GetAllModel().Count;
                int vps = new Vps().GetAllVps().Count;
                int role = new Role().GetAllRole().Count;
                code = "success";
                message = "Thành công";
                dict.Add("timelime", timelime);
                dict.Add("actions", actions);
                dict.Add("account", account);
                dict.Add("link", link);
                dict.Add("comment", comment);
                dict.Add("user", user);
                dict.Add("channel", channel);
                dict.Add("useragent", useragent);
                dict.Add("model", model);
                dict.Add("vps", vps);
                dict.Add("role", role);
            }
            catch (System.Exception ex)
            {
                message = ex.Message;
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }
    }
}