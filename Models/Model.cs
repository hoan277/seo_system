﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Model
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int modelId { get; set; }
        public string modelName { get; set; }
        public string modelDesc { get; set; }
        public string modelCode { get; set; }
        public string modelAccuracy { get; set; }
        public string modelStatus { get; set; }
        public DateTime modelCreated { get; set; }
        public DateTime modelUpdated { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ MODEL =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Model</summary>
        /// <returns>Trả về List Model</returns>
        public List<Model> GetAllModel()
        {
            List<Model> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Model>();
                query.OrderByDescending(x => x.modelName);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Model> List(int _limit, int _offset, string _search)
        {
            List<Model> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Model>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.modelName == search || e.modelName.Contains(search) || e.modelDesc == search || e.modelDesc.Contains(search)
                || e.modelCode == search || e.modelCode.Contains(search) || e.modelId.ToString() == search
                );
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.modelId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ MODEL =====================================

        #region ===================================== XÓA THÔNG QUA modelId=====================================

        /// <summary>Xóa 1 danh mục qua modelId</summary>
        /// <param name="modelId">  Mã modelId của danh mục muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int modelId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Model>().Where(x => x.modelId == modelId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA modelId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Model</summary>
        /// <param name="model">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Model model)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (model.modelId > 0)
                {
                    var queryUpdate = db.From<Model>().Where(e => e.modelId == model.modelId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.modelId = model.modelId;
                        objUpdate.modelName = model.modelName;
                        objUpdate.modelDesc = model.modelDesc;
                        objUpdate.modelCode = model.modelCode;
                        objUpdate.modelAccuracy = model.modelAccuracy;
                        objUpdate.modelStatus = model.modelStatus;
                        objUpdate.modelCreated = model.modelCreated;
                        objUpdate.modelUpdated = model.modelUpdated;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Model
                    {
                        modelName = model.modelName,
                        modelDesc = model.modelDesc,
                        modelCode = model.modelCode,
                        modelAccuracy = model.modelAccuracy,
                        modelStatus = model.modelStatus,
                        modelCreated = DateTime.Now,
                        modelUpdated = DateTime.Now,
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
    }
}