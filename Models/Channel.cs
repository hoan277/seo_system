﻿using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Channel
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int channelId { get; set; }
        public string channelName { get; set; }
        public string channelDesc { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ DANH MỤC =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Danh mục</summary>
        /// <returns>Trả về List Channel</returns>
        public List<Channel> GetAllChannel()
        {
            List<Channel> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Channel>();
                query.OrderByDescending(x => x.channelName);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Channel> List(int _limit, int _offset, string _search)
        {
            List<Channel> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Channel>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.channelName == search || e.channelName.Contains(search) || e.channelDesc == search || e.channelDesc.Contains(search));
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.channelId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ DANH MỤC =====================================

        #region ===================================== XÓA THÔNG QUA channelId=====================================

        /// <summary>Xóa 1 danh mục qua channelId</summary>
        /// <param name="channelId">  Mã channelId của danh mục muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int channelId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Channel>().Where(x => x.channelId == channelId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA channelId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Channel</summary>
        /// <param name="channel">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Channel channel)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (channel.channelId > 0)
                {
                    var queryUpdate = db.From<Channel>().Where(e => e.channelId == channel.channelId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.channelId = channel.channelId;
                        objUpdate.channelName = channel.channelName;
                        objUpdate.channelDesc = channel.channelDesc;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Channel
                    {
                        channelName = channel.channelName,
                        channelDesc = channel.channelDesc
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        #region ===================================== GET =====================================

        /// <summary>
        /// Lấy tất cả danh sách người dùng theo api
        /// </summary>
        /// <returns>Trả về List<User></returns>
        public List<Channel> Get(int channelId, string channelName, string channelDesc)
        {
            List<Channel> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Channel>();
                if (channelId > 0) { query = query.Where(e => e.channelId == channelId); }
                if (!string.IsNullOrEmpty(channelName)) { query = query.Where(e => e.channelName == channelName); }
                if (!string.IsNullOrEmpty(channelDesc)) { query = query.Where(e => e.channelDesc == channelDesc); }
                query.OrderByDescending(x => x.channelId);
                rows = db.Select(query).ToList();
            }
            return rows;
        }

        #endregion ===================================== GET =====================================
    }
}