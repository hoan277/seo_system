﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Vps
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int vpsId { get; set; }
        public string vpsName { get; set; }
        public string vpsPass { get; set; }
        public string vpsIp { get; set; }
        public string vpsAddress { get; set; }
        public string vpsStatus { get; set; }
        public DateTime vpsCreated { get; set; }
        public DateTime vpsUpdated { get; set; }
        public string vpsNote { get; set; }
        public int userId { get; set; }
        public string vpsGroup { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ Vps =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Vps</summary>
        /// <returns>Trả về List Vps</returns>
        public List<Vps> GetAllVps()
        {
            List<Vps> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Vps>();
                query.OrderByDescending(x => x.vpsId);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Vps> Get(int vpsId, string vpsName, string vpsIp, string vpsStatus, string vpsCreated, string vpsNote, string vpsGroup)
        {
            List<Vps> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Vps>();
                if (vpsId > 0) { query = query.Where(e => e.vpsId == vpsId); }
                if (!string.IsNullOrEmpty(vpsName)) { query = query.Where(e => e.vpsName == vpsName); }
                if (!string.IsNullOrEmpty(vpsIp)) { query = query.Where(e => e.vpsIp == vpsIp); }
                if (!string.IsNullOrEmpty(vpsStatus)) { query = query.Where(e => e.vpsStatus == vpsStatus); }
                if (!string.IsNullOrEmpty(vpsCreated)) { query = query.Where(e => e.vpsCreated.ToString() == vpsCreated); }
                if (!string.IsNullOrEmpty(vpsNote)) { query = query.Where(e => e.vpsNote == vpsNote); }
                if (!string.IsNullOrEmpty(vpsGroup)) { query = query.Where(e => e.vpsGroup == vpsGroup); }
                rows = db.Select(query).ToList();
            }
            return rows;
        }


        public List<Vps> List(int _limit, int _offset, string _search)
        {
            List<Vps> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Vps>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.vpsName == search || e.vpsName.Contains(search) || e.vpsId.ToString() == search);
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.vpsId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ Vps =====================================

        #region ===================================== XÓA THÔNG QUA vpsId=====================================

        /// <summary>Xóa 1 Vps qua vpsId</summary>
        /// <param name="vpsId">  Mã vpsId của Vps muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int vpsId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Vps>().Where(x => x.vpsId == vpsId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA vpsId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Vps</summary>
        /// <param name="Vps">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Vps vps)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (vps.vpsId > 0)
                {
                    var queryUpdate = db.From<Vps>().Where(e => e.vpsId == vps.vpsId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.vpsId = vps.vpsId;
                        objUpdate.vpsName = vps.vpsName;
                        objUpdate.vpsPass = vps.vpsPass;
                        objUpdate.vpsIp = vps.vpsIp;
                        objUpdate.vpsAddress = vps.vpsAddress;
                        objUpdate.vpsStatus = vps.vpsStatus;
                        objUpdate.vpsCreated = vps.vpsCreated;
                        objUpdate.vpsUpdated = DateTime.Now;
                        objUpdate.vpsNote = vps.vpsNote;
                        objUpdate.userId = vps.userId;
                        objUpdate.vpsGroup = vps.vpsGroup;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Vps
                    {
                        vpsName = vps.vpsName,
                        vpsPass = vps.vpsPass,
                        vpsIp = vps.vpsIp,
                        vpsAddress = vps.vpsAddress,
                        vpsStatus = vps.vpsStatus,
                        vpsCreated = DateTime.Now,
                        vpsUpdated = DateTime.Now,
                        vpsNote = vps.vpsNote,
                        userId = vps.userId,
                        vpsGroup = vps.vpsGroup,
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        #region ===================================== Batch Delete =====================================

        public Dictionary<object, object> BatchDelete(int[] list_selected)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "error", code = "Chưa có item nào được chọn để xóa";
            if (list_selected.Length > 0)
            {
                for (int i = 0; i < list_selected.Length; i++)
                {
                    Delete(list_selected[i]);
                }
                code = "success";
                message = "Xóa thành công";
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }


        #endregion ===================================== Batch Delete =====================================
        #region ===================================== GetRandom =====================================
        public Vps GetRandom()
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Vps>().Where(e => e.vpsStatus == "active").OrderByRandom();
                return db.Select(query).LastOrDefault();
            }
        }
        #endregion ===================================== GetRandom =====================================

        #region ===================================== BatchUpdateStatus =====================================
        public Dictionary<object, object> BatchUpdateStatus(int[] list_selected, string status)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string code = "error", message = "Thất bại";
            using (var db = DatabaseUtils.OpenConnection())
            {
                for (int i = 0; i < list_selected.Length; i++)
                {
                    var query = db.From<Vps>().Where(e => e.vpsId == list_selected[i]);
                    var obj = db.Select(query).LastOrDefault();
                    if (obj != null)
                    {
                        obj.vpsStatus = status;
                        try
                        {
                            UpdateOrInsert(obj);
                            code = "success";
                            message = "Cập nhật trạng thái thành công";
                        }
                        catch (Exception ex)
                        {
                            code = "error";
                            message = ex.Message;
                            goto lblDone;
                        }
                    }
                }
            }
        lblDone:
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }
        #endregion ===================================== BatchUpdateStatus =====================================
    }
}