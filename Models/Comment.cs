﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Comment
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int comId { get; set; }
        public string comDesc { get; set; }
        public string comStatus { get; set; }
        public string comType { get; set; }
        public int userId { get; set; }
        public string comGroup { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ Comment =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Comment</summary>
        /// <returns>Trả về List Comment</returns>
        public List<Comment> GetAllComment()
        {
            List<Comment> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Comment>();
                query.OrderByDescending(x => x.comId);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Comment> Get(int comId, string comDesc, string comStatus, string comType, string comGroup)
        {
            List<Comment> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Comment>();
                if (comId > 0) { query = query.Where(e => e.comId == comId); }
                if (!string.IsNullOrEmpty(comDesc)) { query = query.Where(e => e.comDesc == comDesc || e.comDesc.Contains(comDesc)); }
                if (!string.IsNullOrEmpty(comStatus)) { query = query.Where(e => e.comStatus == comStatus); }
                if (!string.IsNullOrEmpty(comType)) { query = query.Where(e => e.comType == comType); }
                if (!string.IsNullOrEmpty(comGroup)) { query = query.Where(e => e.comGroup == comGroup); }
                query.OrderByDescending(x => x.comId);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Comment> List(int _limit, int _offset, string _search)
        {
            List<Comment> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Comment>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.comStatus == search || e.comStatus.Contains(search)
                || e.comDesc == search || e.comDesc.Contains(search)
                || e.comId.ToString() == search || e.comType == search || e.comType.Contains(search)
                );
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.comId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ Comment =====================================

        #region ===================================== XÓA THÔNG QUA comId=====================================

        /// <summary>Xóa 1 Comment qua comId</summary>
        /// <param name="comId">  Mã comId của Comment muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int comId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Comment>().Where(x => x.comId == comId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA comId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Comment</summary>
        /// <param name="com">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Comment com)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (com.comId > 0)
                {
                    var queryUpdate = db.From<Comment>().Where(e => e.comId == com.comId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.comId = com.comId;
                        objUpdate.comDesc = com.comDesc;
                        objUpdate.comStatus = com.comStatus;
                        objUpdate.comType = com.comType;
                        objUpdate.userId = com.userId;
                        objUpdate.comGroup = com.comGroup;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Comment
                    {
                        comDesc = com.comDesc,
                        comStatus = com.comStatus,
                        userId = com.userId,
                        comType = com.comType,
                        comGroup = com.comGroup,
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
        #region ===================================== Batch Delete =====================================

        public Dictionary<object, object> BatchDelete(int[] list_selected)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "error", code = "Chưa có item nào được chọn để xóa";
            if (list_selected.Length > 0)
            {
                for (int i = 0; i < list_selected.Length; i++)
                {
                    Delete(list_selected[i]);
                }
                code = "success";
                message = "Xóa thành công";
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        #endregion ===================================== Batch Delete =====================================
        #region ===================================== BatchUpdateStatus =====================================
        public Dictionary<object, object> BatchUpdateStatus(int[] list_selected, string status)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string code = "error", message = "Thất bại";
            using (var db = DatabaseUtils.OpenConnection())
            {
                for (int i = 0; i < list_selected.Length; i++)
                {
                    var query = db.From<Comment>().Where(e => e.comId == list_selected[i]);
                    var obj = db.Select(query).LastOrDefault();
                    if (obj != null)
                    {
                        obj.comStatus = status;
                        try
                        {
                            UpdateOrInsert(obj);
                            code = "success";
                            message = "Cập nhật trạng thái thành công";
                        }
                        catch (Exception ex)
                        {
                            code = "error";
                            message = ex.Message;
                            goto lblDone;
                        }
                    }
                }
            }
        lblDone:
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }
        #endregion ===================================== BatchUpdateStatus =====================================
    }
}