﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Account
    {
        #region ============================================ GETTER & SETTER =====================================

        public int accId { get; set; }
        public string accName { get; set; }
        public string accPass { get; set; }
        public string accDesc { get; set; }
        public string accType { get; set; }
        public string accStatus { get; set; }
        public string accNote { get; set; }
        public int userId { get; set; }
        public string accRecovery { get; set; }
        public string accCookie { get; set; }

        #endregion ============================================ GETTER & SETTER =====================================

        #region ============================================ GET ALL ACCOUNT ========================================

        /// <summary>
        /// Lấy tất cả danh sách tài khoản
        /// </summary>
        /// <returns>Trả về List<Account></returns>
        public List<Account> GetAllAccount()
        {
            List<Account> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Account>();
                query.OrderByDescending(x => x.accName);
                rows = db.Select(query);
            }
            return rows;
        }

        public List<Account> Get(int accId, string accName, string accStatus, string accType, string accNote)
        {
            List<Account> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Account>();
                if (accId > 0) { query = query.Where(a => a.accId == accId); }
                if (!string.IsNullOrEmpty(accName)) { query = query.Where(a => a.accName == accName); }
                if (!string.IsNullOrEmpty(accStatus)) { query = query.Where(a => a.accStatus == accStatus); }
                if (!string.IsNullOrEmpty(accType)) { query = query.Where(a => a.accType == accType); }
                if (!string.IsNullOrEmpty(accNote)) { query = query.Where(a => a.accNote == accNote); }
                query.OrderByDescending(x => x.accId);
                rows = db.Select(query);
            }
            return rows;
        }
        public bool CheckNote(int accId, string accNote)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Account>();
                if (accId > 0) { query = query.Where(a => a.accId == accId); }
                if (!string.IsNullOrEmpty(accName)) { query = query.Where(a => a.accNote == accNote); }
                return db.Select(query).LastOrDefault() != null;
            }
        }


        public Dictionary<object, object> UpdateCookie(int accId, string accCookie)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string code = "error", message = "Thất bại";

            if (accId <= 0) { code = "error"; message = "accId phải lớn hơn 0"; }
            if (string.IsNullOrEmpty(accCookie)) { code = "error"; message = "accCookie không được để trống"; }
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Account>().Where(a => a.accId == accId);
                Account acc = db.Select(query).LastOrDefault();
                if (acc != null)
                {
                    try
                    {
                        acc.accCookie = accCookie;
                        UpdateOrInsert(acc);
                        message = "Cập nhật Cookie thành công cho accId=" + accId;
                        code = "success";
                    }
                    catch (Exception ex)
                    {
                        message = "Lỗi API cập nhật accCookie: " + ex.Message;
                        code = "error";
                    }
                }
                else
                {
                    code = "error";
                    message = "Không tìm thấy tài khoản nào với accId=" + accId;
                }
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }

        public List<Account> List(int _limit, int _offset, string _search)
        {
            List<Account> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Account>();
                int offset = _offset > 0 ? _offset : 0;
                int limit = _limit > 0 ? _limit : 5;
                string search = !string.IsNullOrEmpty(_search) ? _search : "";
                query = query.Where(e => e.accName == search || e.accName.Contains(search) || accDesc == search || e.accDesc.Contains(search) || accType == search || e.accType.Contains(search) || e.accId.ToString() == search);
                query.Skip(offset).Take(limit);
                query.OrderByDescending(x => x.accId);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ============================================ GET ALL ACCOUNT ========================================

        #region ===================================== DELETE A ACCOUNT BY accId=====================================

        /// <summary>
        /// Xóa tài khoản qua accId
        /// </summary>
        /// <param name="accId">The account identifier.</param>
        /// <returns>Trả về ĐÚNG hoặc SAI</returns>
        public bool Delete(int accId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Account>().Where(x => x.accId == accId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        public Account GetAccountById(int accId)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Account>().Where(x => x.accId == accId);
                return db.Select(query).LastOrDefault();
            }
        }

        #endregion ===================================== DELETE A ACCOUNT BY accId=====================================

        #region ===================================== UPDATE OR INSERT =====================================

        /// <summary>
        /// Cập nhật hoặc thêm mới một đối tượng người dùng
        /// </summary>
        /// <param name="acc">Đối tượng Account</param>
        /// <returns>Trả về 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Account acc)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (acc.accId > 0)
                {
                    var queryUpdate = db.From<Account>().Where(e => e.accId == acc.accId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.accId = acc.accId;
                        objUpdate.accName = acc.accName;
                        objUpdate.accPass = acc.accPass;
                        objUpdate.accStatus = acc.accStatus;
                        objUpdate.accDesc = acc.accDesc;
                        objUpdate.accType = acc.accType;
                        objUpdate.accNote = acc.accNote;
                        objUpdate.userId = acc.userId;
                        objUpdate.accRecovery = acc.accRecovery;
                        objUpdate.accCookie = acc.accCookie;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Account
                    {
                        accName = acc.accName,
                        accPass = acc.accPass,
                        accStatus = acc.accStatus,
                        accDesc = acc.accDesc,
                        accType = acc.accType,
                        accNote = acc.accNote,
                        userId = acc.userId,
                        accRecovery = acc.accRecovery,
                        accCookie = acc.accCookie,
                    };
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }

            return result;
        }

        #endregion ===================================== UPDATE OR INSERT =====================================

        public Dictionary<object, object> BatchDelete(int[] list_selected)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string message = "error", code = "Chưa có item nào được chọn để xóa";
            if (list_selected.Length > 0)
            {
                for (int i = 0; i < list_selected.Length; i++)
                {
                    Delete(list_selected[i]);
                }
                code = "success";
                message = "Xóa thành công";
            }
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }
        public Account GetRandomBy(string accType, string accStatus)
        {
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Account>();
                if (!string.IsNullOrEmpty(accType)) { query = query.Where(e => e.accType == accType); }
                if (!string.IsNullOrEmpty(accStatus)) { query = query.Where(e => e.accStatus == accStatus); }
                query.OrderByRandom();
                return db.Select(query).LastOrDefault();
            }
        }
        public Dictionary<object, object> BatchUpdateStatus(int[] list_selected, string status)
        {
            Dictionary<object, object> dict = new Dictionary<object, object>();
            string code = "error", message = "Thất bại";
            using (var db = DatabaseUtils.OpenConnection())
            {
                for (int i = 0; i < list_selected.Length; i++)
                {
                    var query = db.From<Account>().Where(e => e.accId == list_selected[i]);
                    var obj = db.Select(query).LastOrDefault();
                    if (obj != null)
                    {
                        obj.accStatus = status;
                        try
                        {
                            UpdateOrInsert(obj);
                            code = "success";
                            message = "Cập nhật trạng thái thành công";
                        }
                        catch (Exception ex)
                        {
                            code = "error";
                            message = ex.Message;
                            goto lblDone;
                        }
                    }
                }
            }
        lblDone:
            dict.Add("code", code);
            dict.Add("message", message);
            return dict;
        }
    }
}