﻿using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Album
    {
        #region ===================================== GETTERS & SETTERS =====================================
        public int albumId { get; set; }
        public string albumName { get; set; }
        public string albumDesc { get; set; }
        public string albumBackground { get; set; }
        public string albumNote { get; set; }
        public int albumView { get; set; }
        public int userId { get; set; }
        #endregion

        #region ===================================== LẤY TẤT CẢ ALBUM =====================================
        /// <summary>  Trả về một danh sách chứa tất cả Danh mục</summary>
        /// <returns>Trả về List Album</returns>
        public List<Album> GetAllAlbum()
        {
            List<Album> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Album>();
                query.OrderByDescending(x => x.albumName);
                rows = db.Select(query);
            }
            return rows;
        }
        #endregion
        #region ===================================== LẤY TẤT CẢ ALBUM QUA userId =====================================
        /// <summary>  Trả về một danh sách chứa tất cả Danh mục</summary>
        /// <returns>Trả về List Album</returns>
        public List<Album> GetAllAlbumByUserId(int userId)
        {
            List<Album> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Album>().Where(y => y.userId == userId);
                query.OrderByDescending(x => x.albumName);
                rows = db.Select(query);
            }
            return rows;
        }
        #endregion
        #region ===================================== XÓA THÔNG QUA albumId=====================================
        /// <summary>Xóa 1 danh mục qua albumId</summary>
        /// <param name="albumId">  Mã albumId của danh mục muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int albumId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Album>().Where(x => x.albumId == albumId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }
        #endregion
        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
        /// <summary>Cập nhật hoặc thêm mới một đối tượng Album</summary>
        /// <param name="album">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Album album)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (album.albumId > 0)
                {
                    var queryUpdate = db.From<Album>().Where(e => e.albumId == album.albumId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.albumId = album.albumId;
                        objUpdate.albumName = album.albumName;
                        objUpdate.albumDesc = album.albumDesc;
                        objUpdate.albumBackground = album.albumBackground;
                        objUpdate.albumNote = album.albumNote;
                        objUpdate.albumView = album.albumView;
                        objUpdate.userId = album.userId;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Album();
                    objInsert.albumName = album.albumName;
                    objInsert.albumDesc = album.albumDesc;
                    objInsert.albumBackground = album.albumBackground;
                    objInsert.albumNote = album.albumNote;
                    objInsert.albumView = album.albumView;
                    objInsert.userId = album.userId;
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }
        #endregion

    }
}
