﻿using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Models
{
    public class Country
    {
        #region ===================================== GETTERS & SETTERS =====================================

        public int countryId { get; set; }
        public string countryName { get; set; }
        public string countryDesc { get; set; }

        #endregion ===================================== GETTERS & SETTERS =====================================

        #region ===================================== LẤY TẤT CẢ QUỐC GIA =====================================

        /// <summary>  Trả về một danh sách chứa tất cả Danh mục</summary>
        /// <returns>Trả về List Country</returns>
        public List<Country> GetAllCountry()
        {
            List<Country> rows = null;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var query = db.From<Country>();
                query.OrderByDescending(x => x.countryName);
                rows = db.Select(query);
            }
            return rows;
        }

        #endregion ===================================== LẤY TẤT CẢ QUỐC GIA =====================================

        #region ===================================== XÓA THÔNG QUA countryId=====================================

        /// <summary>Xóa 1 danh mục qua countryId</summary>
        /// <param name="countryId">  Mã countryId của danh mục muốn xóa</param>
        /// <returns>Trả về giá trị đúng hoặc sai</returns>
        public bool Delete(int countryId)
        {
            bool delete_rs = false;
            using (var db = DatabaseUtils.OpenConnection())
            {
                var queryDelete = db.From<Country>().Where(x => x.countryId == countryId);
                delete_rs = db.Delete(queryDelete) > 0 ? true : false;
            }
            return delete_rs;
        }

        #endregion ===================================== XÓA THÔNG QUA countryId=====================================

        #region ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================

        /// <summary>Cập nhật hoặc thêm mới một đối tượng Country</summary>
        /// <param name="country">  Truyền vào là một đối tượng</param>
        /// <returns>Giá trị trả về là 1(thành công) hoặc -1(thất bại)</returns>
        public int UpdateOrInsert(Country country)
        {
            int result = -1;
            using (var db = DatabaseUtils.OpenConnection())
            {
                // CODE UPDATE
                if (country.countryId > 0)
                {
                    var queryUpdate = db.From<Country>().Where(e => e.countryId == country.countryId);
                    var objUpdate = db.Select(queryUpdate).SingleOrDefault();
                    if (objUpdate != null)
                    {
                        objUpdate.countryId = country.countryId;
                        objUpdate.countryName = country.countryName;
                        objUpdate.countryDesc = country.countryDesc;
                        result = db.Update(objUpdate);
                    }
                }
                // CODE INSERT
                else
                {
                    var objInsert = new Country();
                    objInsert.countryName = country.countryName;
                    objInsert.countryDesc = country.countryDesc;
                    result = (int)db.Insert(objInsert, selectIdentity: true);
                }
            }
            return result;
        }

        #endregion ===================================== CẬP NHẬT HOẶC THÊM MỚI =====================================
    }
}