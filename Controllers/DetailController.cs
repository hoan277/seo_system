﻿using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    public class DetailController : Controller
    {
        private Detail detailObj = new Detail();

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("~/Detail/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = detailObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = detailObj.GetAllDetail().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/Detail/GetByLogCode")]
        public ActionResult GetByLogCode(string logCode)
        {
            List<Detail> data = new Detail().GetByLogCode(logCode);
            return Json(new { data });
        }

        [HttpGet]
        [Route("~/Detail/GetByLogCodeView")]
        public ActionResult GetByLogCodeView(string logCode)
        {
            List<vw_Detail> data = new Detail().GetByLogCodeView(logCode);
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/Detail/Delete/{detailId}")]
        public JsonResult Delete(int detailId)
        {
            return Json(new { result = detailObj.Delete(detailId) });
        }

        [HttpPost]
        [Route("~/Detail/Update/")]
        public JsonResult Update(Detail detail)
        {
            return Json(new { result = detailObj.UpdateOrInsert(detail) });
        }

        [HttpGet]
        [Route("~/Detail/UpdateTimeEnd")]
        public JsonResult UpdateTimeEnd(int detailId)
        {
            return Json(detailObj.UpdateTimeEnd(detailId));
        }

        [HttpDelete]
        [Route("~/Detail/BatchDelete")]
        public JsonResult BatchDelete(int[] list_selected)
        {
            return Json(new { result = detailObj.BatchDelete(list_selected) });
        }
    }
}