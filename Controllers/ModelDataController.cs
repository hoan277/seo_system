﻿using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Linq;

namespace SEO_System.Controllers
{
    public class ModelDataController : Controller
    {
        private ModelData ModelDataObj = new ModelData();

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("~/ModelData/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = ModelDataObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = ModelDataObj.GetAllModelData().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpDelete]
        [Route("~/ModelData/Delete/{mdId}")]
        public JsonResult Delete(int mdId)
        {
            return Json(new { result = ModelDataObj.Delete(mdId) });
        }

        [HttpPost]
        [Route("~/ModelData/Update/")]
        public JsonResult Update(ModelData md)
        {
            return Json(new { result = ModelDataObj.UpdateOrInsert(md) });
        }
        [HttpGet]
        [Route("~/ModelData/Get/")]
        public JsonResult Get(int mdId)
        {
            return Json(new { result = ModelDataObj.Get(mdId) });
        }
    }
}