﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;

namespace SEO_System.Controllers
{
    public class UserCheckController : Controller
    {
        private UserCheck uaObj = new UserCheck();

        public IActionResult Index()
        {
            if (string.IsNullOrEmpty(HttpContext.Session.GetString("userName")))
            {
                return RedirectToAction("Login", "User");
            }
            return View();
        }

        [HttpGet]
        [Route("~/UserCheck/List")]
        public IActionResult List()
        {
            var data = uaObj.GetAllUserCheck();
            int recordsTotal = uaObj.GetAllUserCheck().Count;
            return Json(new { draw = 1, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }
        [HttpGet]
        [Route("~/UserCheck/ListByStatus")]
        public IActionResult ListByStatus(string type, int status)
        {
            var data = uaObj.GetAllUserByStatus(type, status);
            int recordsTotal = uaObj.GetAllUserByStatus(type, status).Count;
            return Json(new { draw = 1, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }
        [HttpPost]
        [Route("~/UserCheck/UpdateStatus")]
        public ActionResult Update(string type, string email, int status)
        {
            int result = 0;
            if (string.IsNullOrEmpty(email))
            {
                return Json(new { code = 400, msg = "Email is missing", result = result });
            }
            if (string.IsNullOrEmpty(type))
            {
                return Json(new { code = 400, msg = "Type is missing", result = result });
            }
            result = uaObj.UpdateStatus(type, email, status);
            return Json(new { result = result });
        }
        [HttpPost]
        [Route("~/UserCheck/UpdateCookies")]
        public ActionResult UpdateCookies(string email, string cookies)
        {
            int result = 0;
            if (string.IsNullOrEmpty(email))
            {
                return Json(new { code = 400, msg = "Email is missing", result = result });
            }
            if (string.IsNullOrEmpty(cookies))
            {
                return Json(new { code = 400, msg = "Type is missing", result = result });
            }
            result = uaObj.UpdateCookies(email, cookies);
            return Json(new { result = result });
        }
        [HttpDelete]
        [Route("~/UserCheck/Delete/{uaId}")]
        public JsonResult Delete(int uaId)
        {
            return Json(new { result = uaObj.Delete(uaId) });
        }
        [HttpPost]
        [Route("~/UserCheck/Update/")]
        public JsonResult Update(UserCheck ua)
        {
            return Json(new { result = uaObj.UpdateOrInsert(ua) });
        }
    }
}