﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPOI.SS.UserModel;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    public class CommentController : Controller
    {
        private Comment comObj = new Comment();

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("~/Comment/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = comObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = comObj.GetAllComment().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/Comment/Get")]
        public ActionResult Get(int comId, string comDesc, string comStatus, string comType, string comGroup)
        {
            List<Comment> data = new Comment().Get(comId, comDesc, comStatus, comType, comGroup);
            return Json(new { data });
        }
        [HttpDelete]
        [Route("~/Comment/Delete/{comId}")]
        public JsonResult Delete(int comId)
        {
            return Json(new { result = comObj.Delete(comId) });
        }

        [HttpPost]
        [Route("~/Comment/Update/")]
        public JsonResult Update(Comment com)
        {
            return Json(new { result = comObj.UpdateOrInsert(com) });
        }
        [HttpDelete]
        [Route("~/Comment/BatchDelete")]
        public JsonResult BatchDelete(int[] list_selected)
        {
            return Json(new { result = comObj.BatchDelete(list_selected) });
        }

        // ACTION IMPORT EXCEL
        [HttpPost]
        [Route("~/Comment/ImportExcel")]
        public ActionResult ImportExcel()
        {
            IFormFile file = Request.Form.Files[0];
            List<IRow> list_row = new ImportService().ReadExcel(file);
            if (list_row != null)
            {
                var com = new Comment();
                foreach (var row in list_row)
                {
                    int comId = 0;
                    string comDesc = row.Cells[1].ToString();
                    string comType = row.Cells[2].ToString();
                    string comGroup = row.Cells[3].ToString();
                    var check_exist = com.Get(0, comDesc, "", comType, comGroup);
                    if (check_exist.Count > 0)
                    {
                        comId = check_exist[0].comId;
                    }
                    com.UpdateOrInsert(new Comment()
                    {
                        comId = comId,
                        comStatus = "active",
                        comType = comType,
                        comDesc = comDesc,
                        comGroup = comGroup,
                        userId = 1,
                    });
                }
                return Json(new { code = "success", message = "Thêm mới từ Excel thành công" });
            }
            return Json(new { code = "error", message = "Thêm mới từ Excel thất bại" });
        }
        [HttpPost]
        [Route("~/Comment/BatchUpdateStatus")]
        public JsonResult BatchUpdateStatus(int[] list_selected, string status)
        {
            return new JsonResult(comObj.BatchUpdateStatus(list_selected, status));
        }
    }
}