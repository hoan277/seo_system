﻿using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;
namespace SEO_System.Controllers
{
    public class SongController : Controller
    {
        Song songObj = new Song();
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [Route("~/Song/List")]
        public ActionResult List(int length, int start, string search)
        {
            List<Song> data = songObj.GetAllSong();
            return Json(new { data });
        }
        [HttpDelete]
        [Route("~/Song/Delete/{songId}")]
        public JsonResult Delete(int songId)
        {
            return Json(new { result = songObj.Delete(songId) });
        }
        [HttpPost]
        [Route("~/Song/Update/")]
        public JsonResult Update(Song song)
        {
            return Json(new { result = songObj.UpdateOrInsert(song) });
        }
        [HttpGet]
        [Route("~/Song/Edit/{songId}")]
        public PartialViewResult Edit([FromRoute] int songId)
        {
            Song objSong = (new Song()).GetSongBySongId(songId);
            return PartialView("Edit", objSong);
        }
        [HttpPost]
        [Route("~/Song/GetAlbumByUserId/")]
        public JsonResult GetAlbumByUserId(int userId)
        {
            return Json(new { result = new Album().GetAllAlbumByUserId(userId) });
        }
    }
}