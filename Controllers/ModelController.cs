﻿using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    public class ModelController : Controller
    {
        private Model modelObj = new Model();

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("~/Model/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = modelObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = modelObj.GetAllModel().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/Model/ListOld")]
        public ActionResult ListOld()
        {
            List<Model> data = new Model().GetAllModel();
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/Model/Delete/{modelId}")]
        public JsonResult Delete(int modelId)
        {
            return Json(new { result = modelObj.Delete(modelId) });
        }

        [HttpPost]
        [Route("~/Model/Update/")]
        public JsonResult Update(Model model)
        {
            return Json(new { result = modelObj.UpdateOrInsert(model) });
        }
    }
}