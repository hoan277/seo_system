﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    public class LinkController : Controller
    {
        private Link linkObj = new Link();

        public IActionResult Index()
        {
            if (string.IsNullOrEmpty(HttpContext.Session.GetString("userName")))
            {
                return RedirectToAction("Login", "User");
            }
            return View();
        }

        [HttpPost]
        [Route("~/Link/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = linkObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = linkObj.GetAllLink().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/Link/Get")]
        public ActionResult Get(int linkId, string linkName)
        {
            List<Link> data = linkObj.Get(linkId, linkName);
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/Link/Delete/{linkId}")]
        public JsonResult Delete(int linkId)
        {
            return Json(new { result = linkObj.Delete(linkId) });
        }

        [HttpDelete]
        [Route("~/Link/BatchDelete")]
        public JsonResult BatchDelete(int[] list_selected)
        {
            return Json(new { result = linkObj.BatchDelete(list_selected) });
        }

        [HttpPost]
        [Route("~/Link/Update/")]
        public JsonResult Update(Link link)
        {
            return Json(new { result = linkObj.UpdateOrInsert(link) });
        }

        [HttpGet]
        [Route("~/Link/Edit/{linkId}")]
        public PartialViewResult Edit([FromRoute] int linkId)
        {
            return PartialView("Edit", linkObj.GetById(linkId));
        }
    }
}