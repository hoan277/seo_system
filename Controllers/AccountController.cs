﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPOI.SS.UserModel;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    public class AccountController : Controller
    {
        private Account AccountObj = new Account();

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("~/Account/List")]
        public ActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = AccountObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = AccountObj.GetAllAccount().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/Account/Get")]
        public ActionResult List(int accId, string accName, string accStatus, string accType, string accNote)
        {
            List<Account> data = AccountObj.Get(accId, accName, accStatus, accType, accNote);
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/Account/Delete/{accId}")]
        public JsonResult Delete(int accId)
        {
            return Json(new { result = AccountObj.Delete(accId) });
        }

        [HttpDelete]
        [Route("~/Account/BatchDelete")]
        public JsonResult BatchDelete(int[] list_selected)
        {
            return Json(new { result = AccountObj.BatchDelete(list_selected) });
        }

        [HttpPost]
        [Route("~/Account/Update/")]
        public JsonResult Update(Account acc)
        {
            return Json(new { result = AccountObj.UpdateOrInsert(acc) });
        }
        [HttpPost]
        [Route("~/Account/UpdateCookie/")]
        public JsonResult Update(int accId, string accCookie)
        {
            return Json(AccountObj.UpdateCookie(accId, accCookie));
        }

        // ACTION IMPORT EXCEL
        [HttpPost]
        [Route("~/Account/ImportExcel")]
        public ActionResult ImportExcel()
        {
            IFormFile file = Request.Form.Files[0];
            List<IRow> list_row = new ImportService().ReadExcel(file);
            if (list_row != null)
            {
                Account acc = new Account();
                foreach (var row in list_row)
                {
                    int accId = 0;
                    string accName = row.Cells[1].ToString();
                    string accPass = row.Cells[2].ToString();
                    string accType = row.Cells[3].ToString();
                    string accRecovery = row.Cells[4].ToString();
                    var check_exist = acc.Get(0, accName, "", accType, "");
                    if (check_exist.Count > 0)
                    {
                        accId = check_exist[0].accId;
                    }
                    acc.UpdateOrInsert(new Account()
                    {
                        accId = accId,
                        accName = accName,
                        accPass = accPass,
                        accType = accType,
                        accStatus = "active",
                        accRecovery = accRecovery,
                        userId = 1,
                    });
                }
                return Json(new { code = "success", message = "Thêm mới từ Excel thành công" });
            }
            return Json(new { code = "error", message = "Thêm mới từ Excel thất bại" });
        }
        [HttpGet]
        [Route("~/Account/GetRandomBy")]
        public ActionResult List(string accType, string accStatus)
        {
            Account data = AccountObj.GetRandomBy(accType, accStatus);
            return Json(new { data });
        }
        [HttpPost]
        [Route("~/Account/BatchUpdateStatus")]
        public JsonResult BatchUpdateStatus(int[] list_selected, string status)
        {
            return new JsonResult(AccountObj.BatchUpdateStatus(list_selected, status));
        }
    }
}