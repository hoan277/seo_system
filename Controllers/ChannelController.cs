﻿using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    public class ChannelController : Controller
    {
        private Channel channelObj = new Channel();

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("~/Channel/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = channelObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = channelObj.GetAllChannel().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/Channel/ListOld")]
        public ActionResult ListOld()
        {
            List<Channel> data = new Channel().GetAllChannel();
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/Channel/Delete/{channelId}")]
        public JsonResult Delete(int channelId)
        {
            return Json(new { result = channelObj.Delete(channelId) });
        }

        [HttpPost]
        [Route("~/Channel/Update/")]
        public JsonResult Update(Channel channel)
        {
            return Json(new { result = channelObj.UpdateOrInsert(channel) });
        }

        [HttpGet]
        [Route("~/Channel/Get")]
        public ActionResult List(int channelId, string channelName, string channelDesc)
        {
            List<Channel> data = channelObj.Get(channelId, channelName, channelDesc);
            return Json(new { data });
        }
    }
}