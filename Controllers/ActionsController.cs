﻿using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    public class ActionsController : Controller
    {
        private Actions ActionsObj = new Actions();

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Auto()
        {
            return View();
        }

        [HttpPost]
        [Route("~/Actions/List")]
        public ActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = ActionsObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = ActionsObj.GetAllActions().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/Actions/Get")]
        public ActionResult Get(int actId, string actName, string actDesc, string actType, int userId, string actStatus, int timelineId)
        {
            List<Actions> data = ActionsObj.Get(actId, actName, actDesc, actType, userId, actStatus, timelineId);
            return Json(new { data });
        }

        [HttpPost]
        [Route("~/Actions/GetByTimelineId")]
        public ActionResult GetByTimelineId(int timelineId)
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = ActionsObj.GetByTimelineId(int.Parse(length), int.Parse(start), search, timelineId);
            int recordsTotal = data.Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpDelete]
        [Route("~/Actions/Delete/{accId}")]
        public JsonResult Delete(int accId)
        {
            return Json(new { result = ActionsObj.Delete(accId) });
        }

        [HttpPost]
        [Route("~/Actions/Update/")]
        public JsonResult Update(Actions act)
        {
            return Json(new { result = ActionsObj.UpdateOrInsert(act) });
        }
        [HttpPost]
        [Route("~/Actions/GenAuto/")]
        public JsonResult GenAuto(string modeTest, List<string> genPlatform, List<string> genActions, float genAchived, string genLink, string genKeyword, string comGroup, int userId, string actMode, string vpsGroup)
        {
            return Json(ActionsObj.GenAuto(modeTest, genPlatform, genActions, genAchived, genLink, genKeyword, comGroup, userId, actMode, vpsGroup));
        }
    }
}