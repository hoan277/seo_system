﻿using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    public class VpsController : Controller
    {
        private Vps VpsObj = new Vps();

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("~/Vps/List")]
        public ActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = VpsObj.List(int.Parse(length), int.Parse(start), search);
            int recordsTotal = VpsObj.GetAllVps().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/Vps/Get")]
        public JsonResult Get(int vpsId, string vpsName, string vpsIp, string vpsStatus, string vpsCreated, string vpsNote, string vpsGroup)
        {
            List<Vps> data = VpsObj.Get(vpsId, vpsName, vpsIp, vpsStatus, vpsCreated, vpsNote, vpsGroup);
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/Vps/Delete/{VpsId}")]
        public JsonResult Delete(int vpsId)
        {
            return Json(new { result = VpsObj.Delete(vpsId) });
        }

        [HttpDelete]
        [Route("~/Vps/BatchDelete")]
        public JsonResult BatchDelete(int[] list_selected)
        {
            return Json(new { result = VpsObj.BatchDelete(list_selected) });
        }

        [HttpPost]
        [Route("~/Vps/Update/")]
        public JsonResult Update(Vps vps)
        {
            return Json(new { result = VpsObj.UpdateOrInsert(vps) });
        }
        [HttpPost]
        [Route("~/Vps/BatchUpdateStatus")]
        public JsonResult BatchUpdateStatus(int[] list_selected, string status)
        {
            return new JsonResult(VpsObj.BatchUpdateStatus(list_selected, status));
        }
    }
}