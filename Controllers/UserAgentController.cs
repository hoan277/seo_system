﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPOI.SS.UserModel;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    public class UserAgentController : Controller
    {
        private UserAgent uaObj = new UserAgent();

        public IActionResult Index()
        {
            if (string.IsNullOrEmpty(HttpContext.Session.GetString("userName")))
            {
                return RedirectToAction("Login", "User");
            }
            return View();
        }

        [HttpPost]
        [Route("~/UserAgent/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = uaObj.SearchUserAgent(int.Parse(length), int.Parse(start), search);
            int recordsTotal = uaObj.GetAllUserAgent().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/UserAgent/Get")]
        public ActionResult Get(int uaId, string uaDesc, string uaStatus)
        {
            List<UserAgent> data = uaObj.Get(uaId, uaDesc, uaStatus);
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/UserAgent/Delete/{uaId}")]
        public JsonResult Delete(int uaId)
        {
            return Json(new { result = uaObj.Delete(uaId) });
        }

        [HttpDelete]
        [Route("~/UserAgent/BatchDelete")]
        public JsonResult BatchDelete(int[] list_selected)
        {
            return Json(new { result = uaObj.BatchDelete(list_selected) });
        }

        [HttpPost]
        [Route("~/UserAgent/Update/")]
        public JsonResult Update(UserAgent ua)
        {
            return Json(new { result = uaObj.UpdateOrInsert(ua) });
        }

        // ACTION IMPORT EXCEL
        [HttpPost]
        [Route("~/UserAgent/ImportExcel")]
        public ActionResult ImportExcel()
        {
            IFormFile file = Request.Form.Files[0];
            List<IRow> list_row = new ImportService().ReadExcel(file);
            if (list_row != null)
            {
                var ua = new UserAgent();
                foreach (var row in list_row)
                {
                    int uaId = 0;
                    string uaBrowser = row.Cells[1].ToString();
                    string uaDesc = row.Cells[2].ToString();
                    var check_exist = ua.Get(0, uaDesc, "");
                    if (check_exist.Count > 0)
                    {
                        uaId = check_exist[0].uaId;
                    }
                    ua.UpdateOrInsert(new UserAgent()
                    {
                        uaId = uaId,
                        uaStatus = "active",
                        uaBrowser = uaBrowser,
                        uaDesc = uaDesc,
                        userId = 1,
                    });
                }
                return Json(new { code = "success", message = "Thêm mới từ Excel thành công" });
            }
            return Json(new { code = "error", message = "Thêm mới từ Excel thất bại" });
        }
        [HttpPost]
        [Route("~/UserAgent/BatchUpdateStatus")]
        public JsonResult BatchUpdateStatus(int[] list_selected, string status)
        {
            return new JsonResult(uaObj.BatchUpdateStatus(list_selected, status));
        }
    }
}