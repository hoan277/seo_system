﻿using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Linq;

namespace SEO_System.Controllers
{
    public class LogController : Controller
    {
        private Log logObj = new Log();

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("~/Log/UpdateLog")]
        public JsonResult UpdateLog(string logDesc)
        {
            return Json(logObj.UpdateLog(logDesc));
        }

        [HttpGet]
        [Route("~/Log/Get")]
        public JsonResult Get(int logId, string logCode)
        {
            return Json(logObj.Get(logId, logCode));
        }

        [HttpPost]
        [Route("~/Log/List")]
        public ActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = logObj.SearchLog(int.Parse(length), int.Parse(start), search);
            int recordsTotal = logObj.GetAllLog().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpDelete]
        [Route("~/Log/Delete/{logId}")]
        public JsonResult Delete(int logId)
        {
            return Json(new { result = logObj.Delete(logId) });
        }

        [HttpDelete]
        [Route("~/Log/BatchDelete")]
        public JsonResult BatchDelete(int[] list_selected)
        {
            return Json(new { result = logObj.BatchDelete(list_selected) });
        }

        [HttpPost]
        [Route("~/Log/Update/")]
        public JsonResult Update(Log log)
        {
            return Json(new { result = logObj.UpdateOrInsert(log) });
        }
    }
}