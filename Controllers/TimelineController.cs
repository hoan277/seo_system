﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SEO_System.Models;
using System.Collections.Generic;
using System.Linq;

namespace SEO_System.Controllers
{
    public class TimelineController : Controller
    {
        private Timeline timelineObj = new Timeline();

        public IActionResult Index()
        {
            if (string.IsNullOrEmpty(HttpContext.Session.GetString("userName")))
            {
                return RedirectToAction("Login", "User");
            }
            return View();
        }

        [HttpPost]
        [Route("~/Timeline/List")]
        public IActionResult List()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            string search = Request.Form["search[value]"].FirstOrDefault();
            var data = timelineObj.SearchTimeline(int.Parse(length), int.Parse(start), search);
            int recordsTotal = timelineObj.GetAllTimeline().Count;
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }

        [HttpGet]
        [Route("~/Timeline/Get")]
        public ActionResult Get(int timelineId, string timelineName, string timelineStatus, string timelineType)
        {
            List<Timeline> data = timelineObj.Get(timelineId, timelineName, timelineStatus, timelineType);
            return Json(new { data });
        }

        [HttpDelete]
        [Route("~/Timeline/Delete/{timelineId}")]
        public JsonResult Delete(int timelineId)
        {
            return Json(new { result = timelineObj.Delete(timelineId) });
        }

        [HttpDelete]
        [Route("~/Timeline/BatchDelete")]
        public JsonResult BatchDelete(int[] list_selected)
        {
            return Json(new { result = timelineObj.BatchDelete(list_selected) });
        }

        [HttpPost]
        [Route("~/Timeline/Update/")]
        public JsonResult Update(Timeline timeline)
        {
            return Json(new { result = timelineObj.UpdateOrInsert(timeline) });
        }

        [HttpGet]
        [Route("~/Timeline/Edit/{timelineId}")]
        public PartialViewResult Edit([FromRoute] int timelineId)
        {
            return PartialView("Edit", timelineObj.GetById(timelineId));
        }

        [HttpGet]
        [Route("~/Timeline/GetTimelineAction")]
        public JsonResult GetTimelineAction(int timelineId, string timelineStatus, string timelineType, string vpsIp, string actMode)
        {
            return Json(timelineObj.GetTimelineAction(timelineId, timelineStatus, timelineType, vpsIp, actMode));
        }

        [HttpGet]
        [Route("~/Timeline/UpdateStatus")]
        public JsonResult UpdateStatus(int timelineId, string timelineStatus)
        {
            return new JsonResult(timelineObj.UpdateStatus(timelineId, timelineStatus));
        }
        [HttpPost]
        [Route("~/Timeline/BatchDuplicate")]
        public JsonResult BatchDuplicate(int[] list_selected, string[] duplicate_options)
        {
            return new JsonResult(timelineObj.BatchDuplicate(list_selected, duplicate_options));
        }
        [HttpPost]
        [Route("~/Timeline/BatchUpdateStatus")]
        public JsonResult BatchUpdateStatus(int[] list_selected, string status)
        {
            return new JsonResult(timelineObj.BatchUpdateStatus(list_selected, status));
        }
    }
}