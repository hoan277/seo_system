﻿var default_language = {
    "sProcessing": "Đang xử lý...",
    "sLengthMenu": "Xem _MENU_ mục",
    "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
    "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
    "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
    "sInfoFiltered": "(được lọc từ _MAX_ mục)",
    "sInfoPostFix": "",
    "sSearch": "Tìm:",
    "sUrl": "",
    "oPaginate": {
        "sFirst": "Đầu",
        "sPrevious": "Trước",
        "sNext": "Tiếp",
        "sLast": "Cuối"
    },
    "select": {
        "rows": "Đang chọn: %d"
    }
}
var default_button = [
    { extend: 'selectAll', text: 'Select all', charset: 'utf-8', bom: true, init: function (api, node, config) { $(node).addClass("btn-danger"); } },
    { extend: 'selectNone', text: 'Select none', charset: 'utf-8', bom: true },
    { extend: 'excelHtml5', text: 'Excel all', charset: 'utf-8', bom: true, init: function (api, node, config) { $(node).addClass("btn-danger"); }, exportOptions: { modifier: { selected: null } } },
    { extend: 'excelHtml5', text: 'Excel selected', charset: 'utf-8', bom: true, exportOptions: { modifier: { selected: true, } } },
    { extend: 'copyHtml5', text: 'Copy all', charset: 'utf-8', bom: true, init: function (api, node, config) { $(node).addClass("btn-danger"); }, exportOptions: { modifier: { selected: null } } },
    { extend: 'copyHtml5', text: 'Copy selected', charset: 'utf-8', bom: true, exportOptions: { modifier: { selected: true } } },
    { extend: 'print', text: 'Print all', charset: 'utf-8', bom: true, init: function (api, node, config) { $(node).addClass("btn-danger"); }, exportOptions: { modifier: { selected: null } } },
    { extend: 'print', text: 'Print selected', charset: 'utf-8', bom: true, exportOptions: { modifier: { selected: true } } },
]
function show_buttton_actions(buttons) {
    var html_action = '<div class="actions">';
    for (var i = 0; i < buttons.length; i++) {
        switch (buttons[i]) {
            case "save":
                html_action += '<button class="btn btn-sm btn-icon btn-pure btn-success on-editing m-r-5 button-save" title="Lưu" hidden=""><i class="icon-drawer" aria-hidden="true"></i></button>';
                break;
            case "discard":
                html_action += '<button class="btn btn-sm btn-icon btn-pure btn-danger on-editing button-discard" title="Hủy" hidden=""><i class="icon-close" aria-hidden="true"></i></button>';
                break;
            case "edit":
                html_action += '<button class="btn btn-sm btn-icon btn-pure btn-waring on-default m-r-5 button-edit" title="Sửa"><i class="icon-pencil" aria-hidden="true"></i></button>';
                break;
            case "remove":
                html_action += '<button class="btn btn-sm btn-icon btn-pure btn-danger on-default button-remove" title="Xóa"><i class="icon-trash" aria-hidden="true"></i></button>';
                break;
        }
    }
    html_action += '</div>';
    return html_action;
}

function fancy_checkbox(colId) {
    return '<label class="fancy-checkbox"><input id="' + colId + '" class="checkbox-tick disabled" type="checkbox" name="checkbox"><span>' + colId + '</span></label>';
}
function toSlug(title) {
    var slug = "";
    var titleLower = title.toLowerCase();
    slug = titleLower.replace(/e|é|è|ẽ|ẻ|ẹ|ê|ế|ề|ễ|ể|ệ/gi, 'e');
    slug = slug.replace(/a|á|à|ã|ả|ạ|ă|ắ|ằ|ẵ|ẳ|ặ|â|ấ|ầ|ẫ|ẩ|ậ/gi, 'a');
    slug = slug.replace(/o|ó|ò|õ|ỏ|ọ|ô|ố|ồ|ỗ|ổ|ộ|ơ|ớ|ờ|ỡ|ở|ợ/gi, 'o');
    slug = slug.replace(/i|í|ì|ĩ|ị/gi, 'i');
    slug = slug.replace(/y|ý|ỳ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/u|ú|ù|ũ|ủ|ụ|ư|ứ|ừ|ữ|ử|ự/gi, 'u');
    slug = slug.replace(/đ/gi, 'd');
    slug = slug.replace(/\s*$/g, '');
    slug = slug.replace(/\s+/g, '-');
    return slug;
}
function required(inputtx) {
    if (inputtx.value.length == 0) {
        alert("Vui lòng nhập đầy đủ các trường dữ liệu");
        return false;
    }
    return true;
}